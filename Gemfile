source 'https://rubygems.org'


gem 'rails', '4.2.6'
gem 'pg', '~> 0.15'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.1.0'
gem 'bootstrap-sass'
gem 'jquery-rails'
gem 'therubyracer', :platforms => :ruby
gem 'quiet_assets'
gem 'asset_sync'
gem 'nokogiri'
gem 'puma'

# Real time
gem 'pusher'

# Authentication
gem 'devise'

# Show UTC in local time
gem 'local_time'

# # Timezone Detect
# gem 'jstz-rails'

# JS Cookie
gem 'jquery-cookie-rails'

# File uploading
gem 'carrierwave'
# gem 'carrierwave_backgrounder', git: "git://github.com/lardawge/carrierwave_backgrounder.git"
gem 'mini_magick'
gem 'fog'
gem 'remotipart', '~> 1.2'

# Geocoding
gem 'geocoder'
gem 'rubyzip' # maxmind

# Background Jobs
gem 'daemons'
gem 'clockwork'
gem 'sidekiq', '~> 4.0'

# Sidekiq Web
gem 'sinatra', require: false
gem 'slim'

# Google Places Wrapper
gem 'google_places'

# Hashtag
# gem 'simple_hashtag', git: "git://github.com/colewinans/simple_hashtag.git"

# Parse Twitter Mentions
gem 'twitter-text'
gem 'jquery-atwho-rails', "~> 0.3.3"

# Textarea resize
gem 'autosize-rails'

# Pagination
gem 'will_paginate'
gem 'will_paginate-bootstrap'

# Bootstrap Date / Time Pickers
gem 'bootstrap-datepicker-rails'
gem 'bootstrap-timepicker-rails-addon', '~> 0.5.1'

# Timeago helper
gem 'rails-timeago'

# Readmore js
gem 'readmorejs-rails'

# OEmbed and Open Graph Parser
gem 'ruby-oembed'
gem 'opengraph_parser'

# Auto linker
gem 'rails_autolink'

# Private Messaging / Notifcations
gem 'mailboxer'

# Friendly Urls
gem 'friendly_id'

# Search
gem 'searchkick'
gem 'typhoeus'
gem 'faraday_middleware-aws-signers-v4' # for aws

# Activity Feed
gem 'public_activity'

# Mailgun
gem 'mailgun-ruby', require: 'mailgun'

# Mailchimp
gem 'gibbon'

# SVG stuff
gem 'inline_svg'

# Meta tags
gem 'meta-tags', require: 'meta_tags'

# Fancybox Popup
gem 'fancybox2-rails', '~> 0.2.8'

gem 'faker'

# Embedly
gem 'embedly'

# Redis Mutexes
gem 'redis-semaphore'

# DB Dump
gem 'seed_dump'

# For JSON building
gem 'active_model_serializers', '~> 0.10.0'

# Chosen Select
gem 'compass-rails'
gem 'chosen-rails'

gem 'aws-healthcheck'
gem 'rack-ssl-enforcer'

group :development, :test do
  gem 'byebug'
end

group :development do
  gem 'web-console', '~> 2.0'
  gem 'capistrano', '~> 3.4.0'
  gem 'capistrano-rails', '~> 1.1.0'
  gem 'capistrano-bundler'
  gem 'capistrano-rbenv', "~> 2.0" 
  gem 'capistrano3-puma'    
  gem 'capistrano-faster-assets', '~> 1.0'
  gem 'capistrano-secrets-yml', '~> 1.0.0'
end

gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
