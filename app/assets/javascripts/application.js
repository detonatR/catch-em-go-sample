// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require jquery.cookie
//= require bootstrap
//= require bootstrap-datepicker
//= require bootstrap-timepicker
//= require jstz
//= require local_time
//= require jquery.atwho
//= require jquery.autosize
//= require jquery.remotipart
//= require readmore
//= require fancybox
//= require typeahead.bundle.min
//= require leaflet
//= require leaflet-hash
//= require leaflet-fullscreen
//= require chosen-jquery
//= require_tree .

$(document).ready(function(){
  // Set user timezone in cookie
  var tz = jstz.determine();
  $.cookie('timezone', tz.name(), { path: '/' });

  $('.resize').autosize({append:false});
  $(document).ajaxError(function(e, xhr, error) {
    switch(error.status) {
      case 401: {
        location.reload();
        alert(xhr.responseText);
        break;
      }
    }
  });  
});

$(document).on('focus', '.resize', function(e) {
  e.preventDefault;
  $(this).autosize({append:false});
});