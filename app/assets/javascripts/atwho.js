$(document).on('focus', '.atwho', function(event) {
  event.preventDefault();
  var input = $(this);
  input.data('enableEnter', true);

  input.atwho({
    at: "@",
    limit: 6,
    show_the_at: true,
    callbacks: {
      remote_filter: function(query, callback) {
        if (query.length < 1) {
          return false
        }
        $.getJSON("/mentions/users.json", {q: query}, function(data) {
          callback(data);
        });
      }
    }
  });

  input.atwho({
    at: "#",
    limit: 4,
    show_the_at: true,
    callbacks: {
      remote_filter: function(query, callback) {
        if (query.length < 1) {
          return false
        }
        $.getJSON("/hashtags.json", {q: query}, function(data) {
          callback(data);
        });
      }
    }
  });  

  input.on('matched.atwho', function(e) {
    input.data('enableEnter', false);
  });
  input.on('inserted.atwho', function(e) {
    setTimeout(function() {
      input.data('enableEnter', true);
    }, 100);
  });  

});