$(document).ready(function() {
  $(document).on('keydown', '.enter-submit', function(e) {
    var input = $(e.currentTarget);
    if (!$.trim(this.value) == "") {
      if (e.keyCode == 13 && !e.shiftKey && input.data('enableEnter')) {
        e.preventDefault();
        input.closest('form').submit();
      }
    }
  });

  $('.comment-body').readmore({
    lessLink: '<a href="#">See Less</a>',
    moreLink: '<a href="#">See More</a>',
    embedCSS: false,
    collapsedHeight:100
  });  
});

$(document).on('click', 'a.load-more-comments', function(e) {
  e.preventDefault();
  $(this).html("<div class='ajax-loader medium'></div>");
});