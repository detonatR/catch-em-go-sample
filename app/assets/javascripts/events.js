var dateOptions = {
  autoclose: true,
  todayHighlight: true,
  ignoreReadonly: true
}

var timeOptions = {
  template: false
}

$(document).ready(function() {

  $(document).on('change', '#event_image', function(e) {
    $(".banner-ajax-container").show();    
    $(this).parents('form').submit();
  });
  $('.event-description').readmore({
    lessLink: '<a href="#">See Less</a>',
    moreLink: '<a href="#">See More</a>',
    embedCSS: false,
    collapsedHeight:100
  });

  $("#eventModal").on("shown.bs.modal", function() {
    var options = {language: 'en-US', types: ['establishment']};
    var input = $("#place_select");
    var autocomplete = new google.maps.places.Autocomplete(input[0], options);
    google.maps.event.addDomListener(input[0], 'keydown', function(e) {
      if (e.keyCode == 13) {
        e.preventDefault();
        return false;
      }       
    }); 
    geolocate(autocomplete);
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
      var place = autocomplete.getPlace();
      if (place.place_id) {
        $("#place_id").val(place.place_id);
      }      
    })
  });
});

$(document).on("click", ".end-time-add", function() {
  $(".end-time-fields").show();
  $("#event_end_date").datepicker(dateOptions);
  $(".end-time-add").hide();
  $("#event_end_time").timepicker(timeOptions);

  if ($("#event_end_date").val().length == 0) {
    $("#event_end_date").datepicker("setDate", new Date());
  }
  $("#event_end_time").val($("#event_start_time").val());
});

$(document).on("click", ".end-time-remove", function() {
  $(".end-time-fields").hide();
  $("#event_end_date").val('');
  $("#event_end_date").datepicker("clearDates");
  $("#event_end_time").val('');
  $(".end-time-add").show();
});

$("#eventModal").on("show.bs.modal", function(e) {
  $("#event_start_date").datepicker(dateOptions);
  $("#event_start_time").timepicker(timeOptions);

  if ($("#event_start_date").val().length == 0) {
    $("#event_start_date").datepicker("setDate", new Date());
  }  

  if ($("#event_end_date").val().length) {
    $(".end-time-fields").show();
    $("#event_end_date").datepicker(dateOptions);
    $(".end-time-add").hide();
    $("#event_end_time").timepicker(timeOptions);
  }  
});