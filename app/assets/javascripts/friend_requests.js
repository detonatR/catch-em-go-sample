$(document).ready(function() {
  var load_friends = true;
  $(".friends-dropdown").on("shown.bs.dropdown", function() {
    if (load_friends == true) {
      $.ajax({url: "/friend_requests/dropdown"}).done(function() {
        if ($('.friends-menu .pagination').length) {
          var container = $(".friends-dropdown-container");
          container.scroll(function() {
            var url;
            url = $(".friends-menu .pagination .next a").attr('href');
            if (url && container[0].scrollTop + container[0].offsetHeight >= container[0].scrollHeight) {
              $(".friends-menu .pagination").html("<div class='ajax-loader medium'></div>");
              return $.getScript(url);
            }
          });
          return container.scroll();
        }
      });
      load_friends = false;
    }
  });
});