$(document).ready(function() {
  $("#locationModal").on('shown.bs.modal', function () {
    var button = $("#new_location input[type=submit]");
    button.prop('disabled', true);

    var options = {language: 'en-US', types: ['(cities)']};
    var input = $("#location_select");
    var autocomplete = new google.maps.places.Autocomplete(input[0], options);
    google.maps.event.addDomListener(input[0], 'keydown', function(e) {
      if (e.keyCode == 13) {
        e.preventDefault();
        return false;
      }    
    });
    geolocate(autocomplete);
    google.maps.event.addListener(autocomplete, "place_changed", function() {
      fillInLocation(autocomplete);
    });
    input.keyup(function() {
      button.prop('disabled', $.trim(this.value) == "" ? true : false);
    });
  });
});

var locationComponents = {
  locality: 'long_name',
  administrative_area_level_1: 'long_name',
  country: 'long_name'
};

function geolocate(autocomplete) {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete.setBounds(circle.getBounds());
    });
  }
}

function fillInLocation(autocomplete) {
  var place = autocomplete.getPlace();
  if (place.address_components) {
    var lat = place.geometry.location.lat();
    var lng = place.geometry.location.lng();
    var formatted = place.formatted_address
    var addressValues = {};
    for (var i = 0; i < place.address_components.length; i++) {
      var addressType = place.address_components[i].types[0];
      $.each(locationComponents, function(type, label) {
        if (addressType === type) {
          var val = place.address_components[i][label];
          addressValues[type] = val;
        }
      });
    }

    $("#location_formatted_address").val(formatted);
    $("#location_latitude").val(lat);
    $("#location_longitude").val(lng);
    $("#location_city").val(addressValues["locality"]);
    $("#location_state").val(addressValues["administrative_area_level_1"]);
    $("#location_country").val(addressValues["country"])
  }
}