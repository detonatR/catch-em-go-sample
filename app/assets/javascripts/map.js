var map;
var window_hash = window.location.hash;
var defaultLat = "34.009304";
var defaultLng = "-118.497005";
$(document).ready(function() {
  if ($("#map").length) {
    var map_container = $("#map");
    var lat;
    var lng;
    var pokemon = [];
    var gyms = [];
    var pokestops = [];
    var markers = {};

    map = L.map(map_container.get(0), { 
      zoomControl: true,
      zoom: 17,
      fullscreenControl: true
    });
    var hash = new L.Hash(map);
    map.doubleClickZoom.disable();
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
        id: 'mapbox.streets',
        accessToken: 'pk.eyJ1IjoiZGV0b25hdHIiLCJhIjoiY2lyMzV3OXJqMDAydmZubnJtNzRybXo0aCJ9.XA5uyXNwBkCB2B68eRFeow'
    }).addTo(map);
    L.Icon.Default.imagePath = 'https://npmcdn.com/leaflet@1.0.0-rc.3/dist/images';    

    if (window_hash.length) {
      lat = parseHashCoords(window_hash)[0];
      lng = parseHashCoords(window_hash)[1];
      markers.center = L.marker({lat: lat, lng: lng}).addTo(map);
      scanArea(lat, lng);
      startTimer(lat, lng);
    } else {
      map.locate({setView: true, maxZoom: 17});
    }
    map.on('locationfound', onLocationFound);
    map.on('locationerror', onLocationError);

    map.on('fullscreenchange', function () {
      if (!map.isFullscreen()) {
        map.invalidateSize();
      }
    });    
    map.on('dblclick', function(e) {
      markers.center.setLatLng(e.latlng);
      lat = e.latlng.lat;
      lng = e.latlng.lng;
      map.panTo({lat: lat, lng: lng});
      scanArea(lat, lng);       
    });

    var map_search = $(".map-search");
    map_search.each(function() {
      var form = $(this),
          input = form.find('[type=text]'),
          button = form.find('[type=submit]');
      input.on('keyup', function() {
          var val = input.val().trim();
          if (val) {
            button.removeAttr('disabled');
          } else {
            button.attr('disabled', true);
          }
      });
      form.on('submit', function() {
        var val = encodeURIComponent(input.val().trim());
        input.attr('disabled', true);
        button.attr('disabled', true);
        button.html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span>');
        $.post("/map/search/" + val)
          .done(function(data) {
            input.removeAttr('disabled');
            input.val('');
            button.html('<i class="glyphicon glyphicon-search"></i>');
            lat = data.latitude;
            lng = data.longitude;
            map.panTo({lat: data.latitude, lng: data.longitude});
            markers.center.setLatLng({lat: data.latitude, lng: data.longitude});
            updateMarkers();
            scanArea(lat, lng);           
          }).fail(function() {
            input.removeAttr('disabled');
            input.val('');
            button.html('<i class="glyphicon glyphicon-exclamation-sign"></i>')
        });
        return false
      });
    });

    $(".map-locate-me").on('click', function() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(e) {
          lat = e.coords.latitude
          lng = e.coords.longitude
          map.panTo({lat: e.coords.latitude, lng: e.coords.longitude});
          markers.center.setLatLng({lat: e.coords.latitude, lng: e.coords.longitude});
          updateMarkers();
          scanArea(lat, lng);       
        });
      }
      return false
    });


    setInterval(updateMarkers, 1000);
    updateMarkers();

    function findNearbySightings(lat, lng) {
      $.get("/map/sightings/" + lat + "/" + lng)
        .done(function(data) {
          for (var index in data.sightings) {
            var loaded = false;
            for (var m in pokemon) {
              if (pokemon[m].id == data.sightings[index].id) {
                loaded = true;
              }
            }
            if (!loaded) {
              pokemon.push(data.sightings[index]);
            }
          }
          updateMarkers();
        }).fail(function() {

      });
    }

    function findNearbyGyms(lat, lng) {
      $.get("/map/gyms/" + lat + "/" + lng)
        .done(function(data) {
          for (var index in data.gyms) {
            var loaded = false
            for (var m in gyms) {
              if (gyms[m].id == data.gyms[index].id) {
                loaded = true
              }
            }
            if (!loaded) {
              gyms.push(data.gyms[index]);
            }
          }
          updateMarkers();
        }).fail(function() {

      });
    }

    function findNearbyStops(lat, lng) {
      $.get("/map/pokestops/" + lat + "/" + lng)
        .done(function(data) {
          for (var index in data.pokestops) {
            var loaded = false
            for (var m in pokestops) {
              if (pokestops[m].id == data.pokestops[index].id) {
                loaded = true
              }
            }
            if (!loaded) {
              pokestops.push(data.pokestops[index]);
            }
          }
          updateMarkers();
        }).fail(function() {

      });
    }

    function updateMarkers() {
      for (var id in pokemon) {
        var p = pokemon[id],
            expiration = p.expires_at - Math.floor(+new Date() / 1000),
            marker = markers['pokemon-' + id],
            unfiltered = true;

        if (expiration <= 0) {
          if (marker) {
            map.removeLayer(marker);
            delete markers['pokemon-' + id]
          }
          delete pokemon[id];
          continue
        }
        if (!marker) {
          marker = createMarker(id, p, !0, 'pokemon-');
          marker.addedToMap = true;
        } 
        if (unfiltered && !marker.addedToMap) {
          map.addLayer(marker);
          marker.addedToMap = true
        } else if (!unfiltered && marker.addedToMap) {
          map.removeLayer(marker);
          marker.addedToMap = false;
        }
        if (unfiltered) {
          marker.setTooltipContent(secondsToString(expiration));
        }
      }
      for (var id in gyms) {
        var g = gyms[id],
            marker = markers['gyms-' + id],
            unfiltered = true;
        if (!marker) {
          marker = createMarker(id, g, !1, 'gyms-');
          marker.addedToMap = true;
        }
        if (unfiltered && !marker.addedToMap) {
          map.addLayer(marker);
          marker.addedToMap = true;
        } else if (!unfiltered && marker.addedToMap) {
          map.removeLayer(marker);
          marker.addedToMap = false;
        }
      }
      for (var id in pokestops) {
        var s = pokestops[id],
            marker = markers['pokestops-' + id],
            unfiltered = true;
        if (!marker) {
          marker = createMarker(id, s, !1, 'pokestops-');
          marker.addedToMap = true;
        }
        if (unfiltered && !marker.addedToMap) {
          map.addLayer(marker);
          marker.addedToMap = true;
        } else if (!unfiltered && marker.addedToMap) {
          map.removeLayer(marker);
          marker.addedToMap = false;
        }
      }
    }

    function createMarker(id, object, pokemon, marker_name) {
      var icon = L.Icon.extend({
          options: {
            iconUrl: object.image_url,
            shadowUrl: null,
            iconSize: [48, 48],
            iconAnchor: [24, 48],
            wrapperAnchor: [0, 0],
            popupAnchor: [0, -48]
         }
      });       
      var marker = new L.Marker([object.lat, object.lng], {icon: new icon});
      var popup = "<div class='popup'>" +
                    "<div class='popup-image'>" +
                      "<img src='" + object.image_url + "' width='48' height='48'>" +
                    "</div>" +
                    "<div class='popup-info'>" +
                      "<h4>" + object.name + "</h4>" +
                      "<p>Coordinates: " + object.lat + ", " + object.lng + "</p>" +
                      "<p>Added By <a href='/users/" + object.user + "'>" + object.user + "</a></p>" +
                      "<div class='report'><small>Is this marker inaccurate? " + object.report_url + "</small></div>" +
                    "</div>" +
                  "</div>";

      markers[marker_name + id] = marker;
      if (pokemon) {
        var expiration = object.expires_at - Math.floor(+new Date() / 1000);
        var tooltip = L.Tooltip.extend({
          options: {
            permanent: true,
            direction: 'center',
            offset: new L.Point(0,0)
          }
        });        
        marker.addTo(map).bindTooltip(new tooltip).setTooltipContent(secondsToString(expiration));
      } else {
        marker.addTo(map)
      }       
      marker.bindPopup(popup, {minWidth: "320"});
      return marker
    }

    function onLocationFound(event) { 
      lat = event.latlng.lat;
      lng = event.latlng.lng;
      markers.center = L.marker({lat: lat, lng: lng}).addTo(map);
      scanArea(lat, lng);
      startTimer(lat, lng);
    }

    function onLocationError(event) {
      alert("Location Not Found");
      lat = defaultLat;
      lng = defaultLng;
      map.panTo({lat: lat, lng: lng});
      markers.center = L.marker({lat: lat, lng: lng}).addTo(map);
      scanArea(lat, lng);
      startTimer(lat, lng);  
    }

    function scanArea(latitude, longitude) {
      findNearbySightings(latitude, longitude);
      findNearbyGyms(latitude, longitude);
      findNearbyStops(latitude, longitude); 
    }

    function startTimer(latitude, longitude) {
      setInterval(function() {
        findNearbySightings(lat, lng)
      }, 30000);
      setInterval(function() {
        findNearbyGyms(lat, lng);
        findNearbyStops(lat, lng);         
      }, 300000);  
    }      
  }
});

function secondsToString(e) {
  var o = parseInt(e, 10),
      t = Math.floor(o / 3600),
      n = Math.floor((o - (t * 3600)) / 60),
      i = o - (t * 3600) - (n * 60);
  if (t < 10) t = '0' + t;
  if (n < 10) n = '0' + n;
  if (i < 10) i = '0' + i;
  if (t > 0) {
    return t + ':' + n + ':' + i
  };
  return n + ':' + i
}

function parseHashCoords(window_hash) {
  var hash = window_hash.substring(4);
  var match = /^(\-?\d+(\.\d+)?)\/\s*(\-?\d+(\.\d+)?)$/.exec(hash);
  if (!match) {
      map.panTo({lat: defaultLat, lng: defaultLng});
      return [defaultLat, defaultLng];
  }
  return [match[1], match[3]];
}