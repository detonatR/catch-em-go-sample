$(document).ready(function() {
  var load_notifications = true;
  $(".notification-dropdown").on("shown.bs.dropdown", function() {
    if (load_notifications == true) {
      $.ajax({url: "/notifications/dropdown"}).done(function() {
        if ($('.notification-menu .pagination').length) {
          var container = $(".notification-dropdown-container");
          container.scroll(function() {
            var url;
            url = $(".notification-menu .pagination .next a").attr('href');
            if (url && container[0].scrollTop + container[0].offsetHeight >= container[0].scrollHeight) {
              $(".notification-menu .pagination").html("<div class='ajax-loader medium'></div>");
              return $.getScript(url);
            }
          });
          return container.scroll();
        }
      });
      load_notifications = false;
    }
  });
});