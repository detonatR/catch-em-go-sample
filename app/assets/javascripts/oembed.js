var urlregex = new RegExp("(^|[ \t\r\n])((ftp|http|https|gopher|mailto|news|nntp|telnet|wais|file|prospero|aim|webcal):(([A-Za-z0-9$_.+!*(),;/?:@&~=-])|%[A-Fa-f0-9]{2}){2,}(#([a-zA-Z0-9][a-zA-Z0-9$_.+!*(),;/?:@&~=%-]*))?([A-Za-z0-9$_+!*();/?:~-]))","g");

$(document).ready(function() {
  var oembed_field = $("#oembed_url");
  var oembed_container = $("#status_message_body").parents('.module').find('.oembed-preview');
  $(document).on("click", ".card-header .close", function(e){ 
    oembed_field.val("");
    oembed_container.html("");
    oembed_container.hide();
  });

  $("#status_message_body").bind('paste', function(e) {
    var el = $(this)
    setTimeout(function() {
      var url = $(el).val();
      if(url.match(urlregex)) {
        if(!oembed_field.val()) {
          oembed_container.show();
          oembed_container.html("<div class='ajax-loader medium'></div>");
          $.ajax('/embedly', {
            type: 'POST',
            data: {url: $.trim(url.match(urlregex)[0])},
            success: function() {
              oembed_field.val($.trim(url.match(urlregex)[0]));
            }
          });
        }
      }
    }, 100);
  });
});