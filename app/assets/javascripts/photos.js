$(document).ready(function () {
  $(".fancybox").fancybox({
    wrapCSS: "fancybox-image-wrapper",
    padding: 0,
    margin: 0,
    width: '100%',
    height: '100%',
    preload: 0,
    autoCenter: true,
    autoSize: false,
    aspectRatio: false,
    href: $(this.element).attr('href'),
    type: 'ajax',
    afterLoad: function () {
      $.extend(this, {
          aspectRatio: false,
          type: 'html',
          width: '65%',
          height: '65%'
      });
    }
  });
});
