$(document).ready(function() { 
  if ($("body").data("user").length) {
    var pusher = new Pusher("2a32eadcc6a7aacad122", {encrypted: true});
    var username = $('body').data('user');
    var user_channel = pusher.subscribe('private-user-' + username);
    var all_channel = pusher.subscribe("private-main");

    var socket_id = null;

    pusher.connection.bind('connected', function() {
      socket_id = pusher.connection.socket_id;
      $.cookie('socket_id', socket_id, { path: '/' });
    });

    user_channel.bind("pusher:subscription_succeeded", function() {
      user_channel.bind('notification-created', function(data) {
        $.ajax({
          method: "GET",
          url: "/pusher/notifications",
          data: {id: data.id, type: data.type},
          dataType: "script"
        });    
      });

      user_channel.bind('notification-deleted', function(data) {
        // data.notifier_type favorite like
        // type friends notification
        var icon = $("." + data.type + "-toggle .notification-count");
        var notification;

        if (data.type === "friends") {
          notification = $("li[data-friend-username=" + data.username + "]");
        } else {
          notification = $("a[data-notification-id=" + data.id + "]");
        }
        var dropdown = $("." + data.type + "-dropdown-container");
        if (dropdown.length) {
          notification.fadeOut("normal", function() {
            $(this).remove();
          });
        }
        if (icon.length) {
          var counter = icon.text;
          var new_count = counter - 1;
          if (new_count === 0) {
            icon.remove();
          } else {
            icon.text(new_count);
          }
        }
      });

      user_channel.bind('friend-requested', function(data) {
        $.ajax({
          method: "GET",
          url: "/pusher/notifications",
          data: {id: data.id, type: data.type},
          dataType: "script"
        });    
      });

      user_channel.bind('message-created', function(data) {
        $.ajax({
          method: "GET",
          url: "/pusher/notifications",
          data: {id: data.id, type: data.type},
          dataType: "script"
        });     
      });

      user_channel.bind("nearby-post-created", function(data) {
        if ($(".nearby-posts-feed").length) {
          $.ajax({
            method: "GET",
            url: "/pusher/feed",
            data: {id: data.id, type: data.type},
            dataType: "script"
          });       
        }
      }); 

      user_channel.bind("latest-update", function(data) {
        if ($(".feed-latest-updates").length) {
          $.ajax({
            method: "GET",
            url: "/pusher/updates",
            data: {id: data.id},
            dataType: "script"
          });  
        }     
      });   
    });
    
    all_channel.bind("pusher:subscription_succeeded", function() {  
      all_channel.bind("post-created", function(data) {
        if ($(".main-feed").length) {
          $.ajax({
            method: "GET",
            url: "/pusher/feed",
            data: {id: data.id, type: data.type},
            dataType: "script"
          });
        }
      });       
      all_channel.bind("post-deleted", function(data) {
        var post = $("div[data-post-id=" + data.id + "]");
        var shared_posts = $("div[data-shared-id=" + data.id + "]");
        post.closest('.row').fadeOut("normal", function() {
          $(this).remove();
        });
        shared_posts.closest('.row').fadeOut("normal", function() {
          $(this).remove();
        });      
      });
      all_channel.bind("latest-update-deleted", function(data) {
        if ($(".feed-latest-updates").length) {
          var activity = $("div[data-activity-id=" + data.id + "]");
          activity.fadeOut("normal", function() {
            $(this).remove();
          });
        }     
      });   
    });
  }
});