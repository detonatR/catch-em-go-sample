$(document).on('change', '.rsvp-form :input', function() {
  $(this).closest(".rsvp-form").submit();
});

$("#rsvpModal").on("show.bs.modal", function() {
  if ($('.rsvp-list .pagination').length) {
    $('.rsvp-list').scroll(function() {
      var url;
      url = $(".rsvp-list .pagination .next a").attr('href');
      if (url && $(".rsvp-list")[0].scrollTop + $(".rsvp-list")[0].offsetHeight >= $(".rsvp-list")[0].scrollHeight) {
        $(".rsvp-list .pagination").html("<div class='ajax-loader medium'></div>");
        return $.getScript(url);
      }
    });
    return $('.rsvp-list').scroll();
  }
});

$(document).on('click', '.rsvp-menu li a', function(e) {
  e.preventDefault();
  var value = $(this).data("value");
  var button = $(this).closest(".rsvp-button-form").find("input[type=radio]");
  var label = $(this).closest(".rsvp-button-form").find("label");
  button.val(value);
  if (label.hasClass("active")) {
    $(this).closest(".rsvp-button-form").submit();
  } else {
    label.click();
  }
});