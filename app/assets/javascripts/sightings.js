var ready;
ready = function() {
  $("#mapModal a").on("click", function() {
    $("#mapModal .modal-body").html("<div class='ajax-loader medium'></div>");
  });
}

function geocode(latitude, longitude) {
  var geocoder = new google.maps.Geocoder();
  var latlng = {lat: parseFloat(latitude), lng: parseFloat(longitude)};
  geocoder.geocode({'location': latlng}, function(results, status) {
    if (status === google.maps.GeocoderStatus.OK) {
      $("#place_id").val(results[0].place_id);
    } else {
      alert("There was an error, please try again.");
    }
  });
}

$(document).ready(ready);