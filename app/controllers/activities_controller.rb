class ActivitiesController < ApplicationController
  before_filter :authenticate_user!
  include FeedSidebar
  
  def index
    @updates = PublicActivity::Activity.order("id DESC").where(owner_id: current_user.id).page(params[:main_pagination]).per_page(17) 
  end

  def latest_updates 
    @updates = PublicActivity::Activity.order("id DESC").where(owner_id: @user_ids).page(params[:main_pagination]).per_page(17)   
    render template: "activities/index"
  end

end
