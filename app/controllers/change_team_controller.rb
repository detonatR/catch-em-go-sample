class ChangeTeamController < ApplicationController
  before_filter :authenticate_user!
  before_filter :find_profile

  def new
    respond_to do |format|
      format.js
    end
  end

  def create
    if @profile.update(profile_params)
      flash[:notice] = "Your team has successfully been set!"
      render js: "window.location='#{root_path}'"
    else
      respond_to do |format|
        format.js
      end
    end
  end

private
  def find_profile
    @user = current_user
    @profile = @user.profile
  end

  def profile_params
    params.require(:profile).permit(:team_id)
  end
end