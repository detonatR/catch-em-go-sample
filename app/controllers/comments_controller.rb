class CommentsController < ApplicationController
  before_filter :authenticate_user!, except: [:index]
  before_filter :find_commentable

  def index
    @comments = @commentable.comments.page(params[:comments_page]).per_page(5)
    respond_to do |format|
      format.html
      format.js
    end
  end

  def create
    @comment = current_user.comments.new(comment_params)
    if @comment.save

      if @commentable.class.base_class == Post
        @activity = @comment.create_activity :create, owner: current_user, recipient: @commentable.user
        #Push to the post
        PushActivities.perform_later(@activity.id, current_user.id)          
        unless @comment.user == @commentable.user
          @notification = @comment.notifications.create(user: @commentable.user, notifier: @comment.user)
          PushNotifications.perform_later(
            "notifications", @notification.id, @commentable.user.id, "notification-created"
          )       
        end
      end

      @commentable.reload
      respond_to do |format|
        format.js
      end      
      mentions_notification_and_push
    end
  end

  def destroy
    @comment = @commentable.comments.find(params[:id])
    @comment_user = @comment.user
    @notification = @comment.notifications.first
    @activity = @comment.activities.first
    if @comment.destroy
      @commentable.reload
      Pusher["private-user-#{@commentable.user.slug}"].trigger('notification-deleted', {id: @notification.id, type: "notification"}) unless @commentable.user == @comment_user
      Pusher["private-main"].trigger('latest-update-deleted', {id: @activity.id})
      respond_to do |format|
        format.html {redirect_to :back}
        format.js
      end
    end
  end

private

  def find_commentable
    klass = [Post, Photo].detect { |c| params["#{c.name.underscore}_id"]}
    @commentable = klass.find(params["#{klass.name.underscore}_id"])       
  end

  def comment_params
    params.require(:comment).permit(:body).merge(commentable: @commentable)
  end

  def mentions_notification_and_push
    if @comment.mentions.present?
      @comment.mentions.each do |m|
        if m.mentioner.class == Comment
          unless m.mentioner.commentable.class == Photo
            unless m.user == m.mentioner.user
              notification = m.notifications.create(user: m.user, notifier: m.mentioner.user)
              PushNotifications.perform_later(
                "notifications", notification.id, m.user.id, "notification-created"
              )                 
            end
          end
        end
      end
    end
  end
  
end
