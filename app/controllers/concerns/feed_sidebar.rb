module FeedSidebar
  extend ActiveSupport::Concern

  included do 
    before_filter :get_sidebar_info
  end

  def get_sidebar_info
    load_activities if user_signed_in?
    load_trending
  end

  def load_activities
    @profiles = Profile.joins(:location).near([location.latitude, location.longitude])
    @friends = current_user.friends.map {|s| s.id}
    @user_ids = @profiles.map {|s| s.user_id}
    @ids = @friends << @user_ids
    @ids.uniq!
    @activities = PublicActivity::Activity.order("id DESC")
                                          .where(owner_id: @ids).limit(5)
  end

  def load_trending
    @hashtags = Hashtag.trending.limit(5)
  end

end