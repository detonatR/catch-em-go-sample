module Metatags
  extend ActiveSupport::Concern

  KEYWORDS = [
              "Catch 'Em Go", 
              'Pokemon Go', 
              'Pokemon Go Social Network',
              'Pokemon Go GPS', 
              'Pokemon Go Nearby', 
              'Pokemon Go Finder',
              'Pokemon Go Events',
              'Pokemon Go Places',
              'Pokemon Go Meetups',
              'Pokemon Go Crowd Sourced',
              'Pokemon Go Map'
             ]

  included do 
    before_filter :prepare_meta_tags, if: "request.get?"
  end

private
  def prepare_meta_tags(options={})
    site_name = "Catch 'Em Go"
    title = ""
    description = "Connect with trainers from around the world. Share sightings, achievements, events, and more from the field with other Pokemon Go fans."
    image = options[:image] || ActionController::Base.helpers.asset_path("mail/banner.png")
    current_url = request.url

    defaults = {
      site: site_name,
      title: title,
      image: image,
      description: description,
      keywords: Metatags::KEYWORDS,
      charset: "UTF-8",
      reverse: true,
      twitter: {
        site_name: site_name,
        site: "@CatchEmGo",
        card: "summary_large_image",
        description: :description,
        image: image
      },
      og: {
        url: current_url,
        site_name: site_name,
        title: :title,
        image: image,
        description: :description,
        type: 'website'
      }
    }

    options.reverse_merge!(defaults)
    set_meta_tags options
  end

end