class ConversationsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :get_mailbox
  before_filter :get_conversation, only: [:show, :reply]

  def index
  end

  def reply
    @receipt = current_user.reply_to_conversation(@conversation, params[:body])
    @other_user = @conversation.participants.reject {|p| p.id == current_user.id}.first
    PushNotifications.perform_later("conversations", @conversation.id, @other_user.id, "message-created")
    respond_to do |format|
      format.js
    end
  end

  def dropdown
    @mailbox = current_user.mailbox
    @conversations = @mailbox.inbox.order("id DESC").page(params[:conversations_page]).per_page(7)
    respond_to do |format|
      format.js
    end
  end

  def paginate
    @mailbox = current_user.mailbox
    @conversations = @mailbox.inbox.order("id DESC").page(params[:conversations_page]).per_page(7)
    respond_to do |format|
      format.js
    end
  end

private

  def get_mailbox
    @mailbox = current_user.mailbox
    @conversations = @mailbox.conversations
  end

  def get_conversation
    @conversation = @conversations.find(params[:id])
  end

end
