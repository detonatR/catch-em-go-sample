class DashboardController < ApplicationController
  before_filter :authenticate_user!
  include FeedSidebar

  def index
    @posts = current_user.feed.page(params[:main_pagination]).per_page(5)
    @new_post = StatusMessage.new
    @news = NewsFeed.limit(3)
  end
end
