class EventsController < ApplicationController
  before_filter :authenticate_user!, except: [:show]
  before_filter :get_datetimes, only: [:create, :update]
  before_filter :get_place, only: [:create, :update]
  before_filter :get_sidebar_info, only: [:hosting, :rsvps]
  include FeedSidebar
  skip_before_filter :get_sidebar_info, only: [:update_image, :remove_image]
  
  def index
    redirect_to rsvps_events_path
  end

  def show
    @event = Event.friendly.find(params[:id])
    @place = @event.place
    if @place.present?
      @nearby_events = Event.joins(:place).upcoming.recent.near([@place.latitude, @place.longitude], 50).where("events.id not in (?)", @event.id).limit(4)
    end
    @more_events = @event.host.events.upcoming.recent.where("id not in (?)", @event.id).limit(3)
    @posts = @event.posts.page(params[:main_pagination]).per_page(5)
    if user_signed_in?
      @new_post = StatusMessage.new(postable: @event)
      @rsvpable = @event
      if current_user.rsvped_to?(@event.id, @event.class.to_s)
        @rsvp = current_user.rsvps.where(rsvpable_id: @event.id, rsvpable_type: @event.class.to_s).first
      else
        @rsvp = Rsvp.new
      end
    end

    prepare_meta_tags title: @event.name,
                      description: @event.description,
                      image: @event.image? ? @event.image_url(:full) : ActionController::Base.helpers.asset_path("events/banner_placeholder.png")
  end

  def hosting
    @events = current_user.events.recent.page(params[:main_pagination]).per_page(6)
    render template: "events/events"
  end

  def rsvps
    @events = current_user.rsvp_events.recent.where("host_id not in (?)", current_user.id).page(params[:main_pagination]).per_page(6)
    render template: "events/events"
  end

  def new
    @event = Event.new
    respond_to do |format|
      format.js
    end
  end

  def create
    @event = current_user.events.new(@params_with_dates)
    @event.place_id = @place.id if params[:place_select].present? && params[:place_id].present?  
    
    if @event.save
      current_user.rsvp_to!(@event, "yes")        
      flash[:notice] = "You've successfully created your event!"
      render js: "window.location='#{event_path(@event)}'"
    else
      respond_to do |format|
        format.js
      end
    end
  end

  def edit
    @event = current_user.events.friendly.find(params[:id])
  end

  def update
    @event = current_user.events.friendly.find(params[:id])
    @event.place_id = @place.id if params[:place_select].present? && params[:place_id].present? 
    if @event.update(@params_with_dates)        
      flash[:notice] = "You've successfully updated your event!"
      render js: "window.location='#{event_path(@event)}'"
    else
      respond_to do |format|
        format.js
      end
    end    
  end

  def destroy
    @event = current_user.events.friendly.find(params[:id])
    if @event.destroy
      flash[:notice] = "Event has successfully been deleted."
      redirect_to root_path
    end
  end

  def update_image
    @event = current_user.events.friendly.find(params[:event_id])
    if @event.update(params.require(:event).permit(:image))
      respond_to do |format|
        format.js
      end
    end
  end

  def remove_image
    @event = current_user.events.friendly.find(params[:event_id])
    @event.remove_image!
    @event.save
  end

private
  def get_datetimes
    start_fix = DateTime.strptime(params[:event][:start_date], "%m/%d/%Y").to_date
    combined_start = "#{start_fix} #{params[:event][:start_time]}"
    proper_start = ActiveSupport::TimeZone[Time.zone.name].parse(combined_start).utc

    if params[:event][:end_date].present?
      end_fix = DateTime.strptime(params[:event][:end_date], "%m/%d/%Y").to_date
      combined_end = "#{end_fix} #{params[:event][:end_time]}"
      proper_end = ActiveSupport::TimeZone[Time.zone.name].parse(combined_end).utc
    end

    @params_with_dates = event_params.merge(start: proper_start, end: params[:event][:end_date].present? ? proper_end : nil)
  end

  def get_place
    if params[:place_select].present? && params[:place_id].present?
      if place = Place.find_by(google_place_id: params[:place_id])
        @place = place
      else
        @place = Place.create(google_place_id: params[:place_id])
        GooglePlaceDetails.perform_later(@place.id)
      end
    end    
  end

  def event_params
    params.require(:event).permit(:name, :image, :description, :start, :end, :hidden, :category, :place_id)
  end  
end