class FavoritesController < ApplicationController
  before_filter :authenticate_user!
  before_filter :find_favoritable

  def index
  end

  def create
    @favorite = current_user.favorites.new(favoritable: @favoritable)
    if @favorite.save
      @activity = @favorite.create_activity :create, owner: current_user, recipient: @favoritable.user
      PushActivities.perform_later(@activity.id, current_user.id) 
      unless @favorite.user == @favoritable.user
        @notification = @favorite.notifications.create(user: @favoritable.user, notifier: @favorite.user) unless @favorite.user == @favoritable.user
        PushNotifications.perform_later("notifications", @notification.id, @favoritable.user.id, "notification-created")
      end       
      respond_to do |format|
        format.js
      end     
    end    
  end

  def destroy
    @favorite = current_user.favorites.where(favoritable_id: @favoritable.id, favoritable_type: @favoritable.class.base_class.to_s).first
    @favorite_user = @favorite.user
    @notification = @favorite.notifications.first
    @activity = @favorite.activities.first
    if @favorite.destroy
      Pusher["private-user-#{@favoritable.user.slug}"].trigger('notification-deleted', {id: @notification.id, type: "notification"}) unless @favoritable.user == @favorite_user
      Pusher["private-main"].trigger('latest-update-deleted', {id: @activity.id})
      respond_to do |format|
        format.html {redirect_to :back}
        format.js
      end
    end    
  end

private
  def find_favoritable
    klass = [Post].detect { |c| params["#{c.name.underscore}_id"]}
    @favoritable = klass.find(params["#{klass.name.underscore}_id"]) 
  end  
end
