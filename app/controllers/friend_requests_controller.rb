class FriendRequestsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_friend_request, only: [:update, :destroy]
  include FeedSidebar
  skip_before_action :get_sidebar_info, except: [:index]
  
  def index
    @incoming = FriendRequest.where(friend: current_user)
    @incoming = @incoming.page(params[:main_pagination]).per_page(8)
    #@outgoing = current_user.friend_requests
  end  

  def create
    @user = User.friendly.find(params[:friend_id])
    @friend_request = current_user.friend_requests.new(friend: @user)
    if @friend_request.save
      PushNotifications.perform_later("friend_requests", @friend_request.id, @user.id, "friend-requested")
      respond_to do |format|
        format.js
      end
    end
  end

  def update
    @friend_request.accept!
    respond_to do |format|
      format.js
    end
  end

  def destroy
    @friend_request.destroy
    Pusher["private-user-#{@user.slug}"].trigger('notification-deleted', {username: @user.slug, type: "friends"})
    respond_to do |format|
      format.js
    end
  end

  def dropdown
    @friend_requests = FriendRequest.where(friend: current_user).order('id DESC')
    @friend_requests = @friend_requests.page(params[:friend_requests_page]).per_page(7)
    respond_to do |format|
      format.js
    end
  end

  def paginate
    @friend_requests = FriendRequest.where(friend: current_user).order('id DESC')
    @friend_requests = @friend_requests.page(params[:friend_requests_page]).per_page(7)
    respond_to do |format|
      format.js
    end
  end

private

  def set_friend_request
    @user = User.friendly.find(params[:id])
    @friend_request = FriendRequest.find_by(user: @user, friend: current_user) || FriendRequest.find_by(user: current_user, friend: @user)   
  end
end