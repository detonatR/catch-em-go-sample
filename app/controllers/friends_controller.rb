class FriendsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_friend, only: :destroy

  def index
    @user = User.confirmed.friendly.find(params[:user_id].downcase)
    @friends = @user.friends
  end

  def destroy
    current_user.remove_friend(@friend)
    respond_to do |format|
      format.js
    end
  end

private

  def set_friend
    @friend = current_user.friends.friendly.find(params[:id])
  end
end