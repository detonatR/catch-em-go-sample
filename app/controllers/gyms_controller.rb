class GymsController < ApplicationController
  before_filter :authenticate_user!

  def new
    @gym = current_user.gyms.new
    @gym.build_marker
    respond_to do |format|
      format.js
    end
  end

  def create
    @gym = current_user.gyms.new(gym_params)
    if @gym.save
      @gym.marker.set_place(params[:place_id])
      @activity = @gym.create_activity :create, owner: current_user
      PushActivities.perform_later(@activity.id, current_user.id, true, {lat: location.latitude, lng: location.longitude})       
      flash[:notice] = "Gym has successful been saved!"
      render js: "window.location='#{root_path}'"
    else
      respond_to do |format|
        format.js
      end
    end
  end

private
  def gym_params
    params.require(:gym).permit(:name, marker_attributes: [:latitude, :longitude])
  end
end