class HashtagsController < ApplicationController
  before_filter :authenticate_user!, only: [:index]
  include FeedSidebar
  
  def index
    respond_to do |format|
      format.html { redirect_to root_path }
      format.json { render json: Hashtag.where('name like ?', "#{params[:q]}%").map {|h| {name: h.display} } }
    end
  end

  def show
    @hashtag = Hashtag.find_by_name(params[:id])
    if @hashtag
      @hashtagged = @hashtag.hashtaggables.sort_by(&:created_at).reverse 
      @hashtagged = @hashtagged.paginate(page: params[:page], per_page: 5)
    end
    @news = NewsFeed.limit(3)
    @page_title = "##{@hashtag.display}"
    @page_description = "See what others are saying about ##{@hashtag.display} on Catch 'Em Go."
  end

end
