class HomeController < ApplicationController
  before_filter :set_footer_and_bg

  def index
    @news = NewsFeed.limit(3)
    @events = Event.featured.upcoming.recent.limit(3)
    @hashtags = Hashtag.trending.limit(4)
  end

private
  def set_footer_and_bg
    @home_page = true
    @footer_page = true    
  end  
end