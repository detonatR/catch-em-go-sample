class LikesController < ApplicationController
  before_filter :authenticate_user!
  before_filter :find_likeable

  def index
  end

  def create
    @like = current_user.likes.new(likeable: @likeable)
    if @like.save
      if @likeable.class.base_class == Post
        @activity = @like.create_activity :create, owner: current_user, recipient: @likeable.user
        PushActivities.perform_later(@activity.id, current_user.id) 
        unless @like.user == @likeable.user
          @notification = @like.notifications.create(user: @likeable.user, notifier: @like.user)
          PushNotifications.perform_later("notifications", @notification.id, @likeable.user.id, "notification-created")     
        end  
      end      
      @likeable.reload
      respond_to do |format|
        format.js
      end      
    end
  end

  def destroy
    @like = current_user.likes.where(likeable_id: @likeable.id, likeable_type: @likeable.class.base_class.to_s).first
    @like_user = @like.user
    @notification = @like.notifications.first
    @activity = @like.activities.first
    if @like.destroy
      @likeable.reload
      Pusher["private-user-#{@likeable.user.slug}"].trigger('notification-deleted', {id: @notification.id, type: "notification"}) unless @likeable.user == @like_user
      Pusher["private-main"].trigger('latest-update-deleted', {id: @activity.id})
      respond_to do |format|
        format.html {redirect_to :back}
        format.js
      end
    end
  end

private
  def find_likeable
    klass = [Photo, Post, Comment].detect { |c| params["#{c.name.underscore}_id"]}
    @likeable = klass.find(params["#{klass.name.underscore}_id"])    
  end
end
