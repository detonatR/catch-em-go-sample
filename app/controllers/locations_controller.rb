class LocationsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :find_locatable

  def new
    @location = Location.new
    respond_to do |format|
      format.js
    end    
  end

  # this code wont work for photos or posts

  def create
    @location = Location.find_or_create_by(location_params)
    @locatable.location = @location
    if @locatable.save      
      flash[:notice] = "Location successfully changed"
      render js: "window.location='#{root_path}'"
    else
      respond_to do |format|
        format.js
      end
    end
  end
  
private
  def find_locatable
    klass = [User, Post, Photo].detect { |c| params["#{c.name.underscore}_id"]}
    if klass = User
      @locatable = current_user.profile
    else
      @locatable = klass.find(params["#{klass.name.underscore}_id"])
    end       
  end

  def location_params
    params.require(:location).permit(:city, :state, :country, :latitude, :longitude, :formatted_address)
  end
end
