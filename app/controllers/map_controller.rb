class MapController < ApplicationController
  def sightings
    @sightings = Sighting.visible.visible_to(current_user).active.joins(:marker).near([params[:lat], params[:lng]])    
    render json: @sightings
  end

  def gyms
    @gyms = Gym.visible.visible_to(current_user).joins(:marker).near([params[:lat], params[:lng]])    
    render json: @gyms
  end

  def pokestops
    @pokestops = Pokestop.visible.visible_to(current_user).joins(:marker).near([params[:lat], params[:lng]])
    render json: @pokestops
  end

  def search
    @location = params[:id]
    @results = Geocoder.search(params[:id])
    if @results.present?
      @r = @results.first
      render json: {latitude: @r.latitude, longitude: @r.longitude}
    else
      render status: 400, nothing: true
    end
  end
end