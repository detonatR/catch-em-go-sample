class MentionsController < ApplicationController
  before_filter :authenticate_user!
  include FeedSidebar
  skip_before_filter :get_sidebar_info, only: [:users]
  
  def index
    @mentions = current_user.mentions.where(mentioner_type: "Post").page(params[:page]).per_page(5)
  end

  def users
    respond_to do |format|
      format.json { render json: User.confirmed.where('slug like ?', "#{params[:q]}%").map {|u| 
          {
            name: u.username
          } 
        } 
      }
    end       
  end
end