class PhotosController < ApplicationController
  before_filter :authenticate_user!
  def index
    @user = User.friendly.find(params[:user_id]) 
    @photos = @user.photos
  end

  def show
    @photo = Photo.find(params[:id])
    @user = @photo.user
    @comments = @photo.comments.page(params[:comments_page]).per_page(5)
    render layout: false
  end

  def new
    @photo = Photo.new
    respond_to do |format|
      format.html {redirect_to root_path}
      format.js
    end    
  end

  def create
    @photo = current_user.photos.new(photo_params)
    if @photo.save
      respond_to do |format|
        format.html {redirect_to :back}
        format.js
      end
    end
  end

  def destroy
    @photo = current_user.photos.find(params[:id])
    if @photo.destroy
      flash[:notice] = "Photo has successfully been deleted."
      redirect_to user_photos_path(current_user)
    end
  end

private

  def photo_params
    params.require(:photo).permit(:description, :image)
  end
end
