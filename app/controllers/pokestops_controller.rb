class PokestopsController < ApplicationController
  before_filter :authenticate_user!
  
  def new
    @pokestop = current_user.pokestops.new
    @pokestop.build_marker
    respond_to do |format|
      format.js
    end
  end

  def create
    @pokestop = current_user.pokestops.new(pokestop_params)
    if @pokestop.save
      @pokestop.marker.set_place(params[:place_id])
      @activity = @pokestop.create_activity :create, owner: current_user
      PushActivities.perform_later(@activity.id, current_user.id, true, {lat: location.latitude, lng: location.longitude})             
      flash[:notice] = "PokeStop has successful been saved!"
      render js: "window.location='#{root_path}'"
    else
      respond_to do |format|
        format.js
      end
    end    
  end

private
  def pokestop_params
    params.require(:pokestop).permit(:name, marker_attributes: [:latitude, :longitude])
  end
end