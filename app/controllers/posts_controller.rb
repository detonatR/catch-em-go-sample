class PostsController < ApplicationController
  before_filter :authenticate_user!, except: [:show]
  include FeedSidebar
  skip_before_filter :get_sidebar_info, only: [:embedly]  

  def show
    @post = Post.find(params[:id])
    @user = @post.user
    @news = NewsFeed.limit(3)
    prepare_meta_tags title: "#{@user.full_name}: #{@post.body}",
                      description: @post.body,
                      image: @user.profile.image? ? @user.profile.image_url(:thumb_large) : ActionController::Base.helpers.asset_path("male_user.png"),
                      twitter: {card: "summary"}
  end

  def update
    
  end

  def destroy
    @post = current_user.posts.find(params[:id])
    @post_id = @post.id
    if @post.destroy
      Pusher.trigger('private-main', 'post-deleted', {id: @post_id}, {socket_id: cookies[:socket_id]})
      respond_to do |format|
        format.html {redirect_to :back}
        format.js
      end
    end
  end

  def embedly
    @url = params[:url]
    @embedly = OEmbedCache.find_or_create_by(@url)
    if @embedly 
      respond_to do |format|
        format.js
      end
    end
  end
end
