class ProfilesController < ApplicationController
  before_filter :authenticate_user!
  before_filter :get_current_user, only: [:edit, :update, :remove_avatar, 
                :update_avatar, :edit_header, :update_header]
  before_filter :find_user, only: [:show, :favorites]

  def edit
  end

  def update
    if @profile.update(profile_params)
      respond_to do |format|
        format.js
      end
    end
  end

  def show
    @footer_page = true    
    @profile = @user.profile
  end

  def favorites
    @favorites = @user.favorites.page(params[:main_pagination]).per_page(5)
    respond_to do |format|
      format.html
      format.js
    end
  end

  def remove_avatar
    @profile.remove_image!
    @profile.save
  end

  def update_avatar
    if @profile.update(params.require(:profile).permit(:image))
      respond_to do |format|
        format.js
      end
    end
  end

  def edit_header 
  end

  def update_header
    if @profile.update(params.require(:profile).permit(:snippet))
      respond_to do |format|
        format.js
      end
    end    
  end

private

  def get_current_user
    @user = current_user
    @profile = @user.profile
  end

  def find_user
    @user = User.friendly.find(params[:user_id])
  end

  def profile_params
    params.require(:profile).permit(:about, :gender, :birthday)
  end

end
