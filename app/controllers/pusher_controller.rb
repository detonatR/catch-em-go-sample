 class PusherController < ApplicationController
  protect_from_forgery except: :auth
  before_filter :authenticate_user!, only: [:notifications, :feed, :updates, :unrequest]
  def auth
    channel = params[:channel_name]
    socket_id = params[:socket_id]

    if current_user 
      return pusher_forbidden if channel =~ /user/ && current_user.slug != channel.split("user-")[1]
      pusher_token
    else
      pusher_forbidden
    end
  end

  def notifications
    @type = params[:type]
    if @type == "conversations"
      @object = Mailboxer::Conversation.find(params[:id])
    else
      klass = @type.classify.safe_constantize
      @object = klass.find(params[:id])
    end
    
    @icon_partial = "#{@type}/icon"
    case @type
    when "notifications"
      @partial = "notifications/#{@object.notifiable_type.underscore}/dropdown"
    when "friend_requests"
      @partial = "friend_requests/dropdown/friend_request"
    when "conversations"
      @partial = "conversations/dropdown/conversation"
    end
  end

  def feed
    @post = Post.find(params[:id])
    @type = params[:type]
  end

  def updates
    @activity = PublicActivity::Activity.find(params[:id])
  end

private
  def pusher_token
    response = Pusher[params[:channel_name]].authenticate params[:socket_id]
    render json: response    
  end

  def pusher_forbidden
    render text: "Not authorized", status: '403'
  end
end