class ReportsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :find_reportable

  def create
    @report = current_user.reports.new(reportable: @reportable)
    if @report.save
      flash[:notice] = "Thank you for reporting this. If you need additional help, please contact us."
      render js: "window.location='#{root_path}'"
    else
      render status: 400, nothing: true
    end
  end

private
  def find_reportable
    klass = [Sighting, Gym, Pokestop].detect { |c| params["#{c.name.underscore}_id"]}
    @reportable = klass.friendly.find(params["#{klass.name.underscore}_id"])         
  end
end
