class RsvpsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :find_rsvpable

  def index
    @rsvps = @rsvpable.rsvps.where(status: params[:status]).page(params[:rsvps_pagination]).per_page(6)
    respond_to do |format|
      format.js
    end    
  end

  def create
    @rsvp = current_user.rsvps.new(rsvp_params.merge(rsvpable: @rsvpable))
    if @rsvp.save
      @activity = @rsvp.create_activity :create, owner: current_user, recipient: @rsvpable.host
      PushActivities.perform_later(@activity.id, current_user.id) 
      unless @rsvp.user == @rsvpable.host
        @notification = @rsvp.notifications.create(user: @rsvpable.host, notifier: @rsvp.user)
        PushNotifications.perform_later("notifications", @notification.id, @rsvpable.host.id, "notification-created")
      end      
      @rsvpable.reload
      respond_to do |format|
        format.js
      end      
    end
  end

  def update
    @rsvp = current_user.rsvps.find(params[:id])
    if @rsvp.update(rsvp_params)
      @rsvpable.reload
      respond_to do |format|
        format.js
      end
    end
  end

  def destroy
    
  end

  def paginate
    @rsvp = @rsvpable.rsvps.where(status: params[:status]).page(params[:rsvps_pagination]).per_page(6)
    respond_to do |format|
      format.js
    end    
  end

private
  def find_rsvpable
    klass = [Event].detect {|c| params["#{c.name.underscore}_id"]}
    @rsvpable = klass.friendly.find(params["#{klass.name.underscore}_id"])    
  end

  def rsvp_params
    params.require(:rsvp).permit(:status)
  end 
end