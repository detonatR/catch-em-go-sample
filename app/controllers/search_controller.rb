class SearchController < ApplicationController
  skip_before_filter :get_sidebar_info, only: [:users, :hashtags]
  include FeedSidebar

  def index
    @users = User.confirmed.search params[:q], autocomplete: true
    #@hashtags = SimpleHashtag::Hashtag.search params[:q], autocomplete: true
    @news = NewsFeed.limit(3)
    @page_title = "#{params[:q]} - Search Results"
    @page_description = "Search results for #{params[:q]} on Catch 'Em Go."
  end

  def users
    @users = User.confirmed.search(params[:query], autocomplete: true, limit: 10)
    render json: @users.map {|u| 
        { 
          id: u.id,
          name: u.full_name, 
          username: u.username, 
          image_url: u.profile.image? ? u.profile.image_url(:thumb_small) : ActionController::Base.helpers.asset_path("small_male_user.png"), 
          formatted_address: u.profile.location.present? ? u.profile.location.formatted_address : "No Location", 
          team_image_url: u.profile.team.present? ? u.profile.team.image_url(:flair) : ActionController::Base.helpers.asset_path("no_team.png") 
        }
     }.to_json
  end

  def hashtags
    @hashtags = Hashtag.search(params[:query], autocomplete: true, limit: 10)
    render json: @hashtags.map {|h| {name: h.display}}.to_json
  end

end