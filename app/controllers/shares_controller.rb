class SharesController < ApplicationController
  before_filter :authenticate_user!
  before_filter :find_shareable

  def new
    @share = Share.new
    respond_to do |format|
      format.html {redirect_to root_path}
      format.js
    end
  end
  
  def create
    if @shareable.is_a? Share
      @absolute_root = @shareable.absolute_root
      @share = current_user.shares.new(share_params.merge(shareable: @absolute_root))
    else
      @absolute_root = @shareable
      @share = current_user.shares.new(share_params.merge(shareable: @absolute_root))
    end

    if @share.save
      PushPosts.perform_later("share", @share.id, cookies[:socket_id])
      PushPosts.perform_later("share", @share.id, nil, {lat: location.latitude, lng: location.longitude})
      unless @share.user == @absolute_root.user
        @notification = @share.notifications.create(user: @absolute_root.user, notifier: @share.user)
        PushNotifications.perform_later(
                "notifications", @notification.id, @absolute_root.user.id, "notification-created"
              )   
      end      
      @absolute_root.reload
       respond_to do |format|
         format.js
       end
       mentions_notification_and_push
            
    else
      render nothing: true, status: 422
    end
  end

  def destroy
    if @shareable.is_a? Share
      @absolute_root = @shareable.absolute_root
      @share = current_user.shares.where(shareable_id: @absolute_root.id, shareable_type: @absolute_root.class.base_class.to_s).first
    else
      @absolute_root = @shareable
      @share = current_user.shares.where(shareable_id: @absolute_root.id, shareable_type: @absolute_root.class.base_class.to_s).first
    end
    @current_user_shared_id = @share.id
    @share_user = @share.user
    @notification = @share.notifications.first
    if @share.destroy
      Pusher.trigger('private-main', 'post-deleted', {id: @current_user_shared_id}, {socket_id: cookies[:socket_id]})
      Pusher["private-user-#{@absolute_root.user.slug}"].trigger('notification-deleted', {id: @notification.id, type: "notification"}) unless @absolute_root.user == @share_user
      @absolute_root.reload
      respond_to do |format|
        format.html {redirect_to :back}
        format.js
      end
    end
  end

private

  def find_shareable
    klass = [Post].detect { |c| params["#{c.name.underscore}_id"]}
    @shareable = klass.find(params["#{klass.name.underscore}_id"])       
  end  

  def share_params
    params.require(:share).permit(:body)
  end

  def mentions_notification_and_push
    if @share.mentions.present?
      @share.mentions.each do |m|
        unless m.user == m.mentioner.user
          notification = m.notifications.create(user: m.user, notifier: m.mentioner.user)
          PushNotifications.perform_later(
                "notifications", notification.id, m.user.id, "notification-created"
              )   
        end
      end
    end
  end  
end