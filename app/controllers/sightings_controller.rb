class SightingsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :get_datetime, only: [:create]  

  def new
    @sighting = current_user.sightings.new
    @sighting.build_marker
    respond_to do |format|
      format.js
    end
  end

  def create
    @sighting = current_user.sightings.new(@params_with_date)
    if @sighting.save
      @sighting.marker.set_place(params[:place_id])
      @activity = @sighting.create_activity :create, owner: current_user
      PushActivities.perform_later(@activity.id, current_user.id, true, {lat: location.latitude, lng: location.longitude})  
      flash[:notice] = "Sighting has successful been saved!"
      render js: "window.location='#{root_path}'"
    else
      respond_to do |format|
        format.js
      end
    end
  end

private

  def get_datetime
    seen_fix = DateTime.strptime(params[:sighting][:seen_date], "%m/%d/%Y").to_date
    combined_seen = "#{seen_fix} #{params[:sighting][:seen_time]}"
    seen = ActiveSupport::TimeZone[Time.zone.name].parse(combined_seen).utc
    @params_with_date = sighting_params.merge(seen_at: seen)
  end

  def sighting_params
    params.require(:sighting).permit(:pokemon_id, :seen_at, marker_attributes: [:latitude, :longitude])
  end
  
end