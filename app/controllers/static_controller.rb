class StaticController < ApplicationController
  before_filter :set_footer_and_bg

  def index
    @user = User.new
    @user.build_profile
    @page_title = "Login or Sign Up"
  end

  def about
    @page_title = "About"
  end  

  def contact
    @page_title = "Contact"
  end

  def privacy
    @page_title = "Privacy Policy"
  end

  def tos
    @page_title = "Terms of Service"
  end

private
  def set_footer_and_bg
    @home_page = true
    @footer_page = true    
  end
end
