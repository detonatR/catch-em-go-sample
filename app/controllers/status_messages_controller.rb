class StatusMessagesController < ApplicationController
  before_filter :authenticate_user!
  before_filter :find_postable, only: [:create]

  def create
    @status_message = current_user.status_messages.new(status_params)
    @status_message.attach_oembed_data(params[:oembed_url]) if oembed_included?
    if @status_message.save
      PushPosts.perform_later("status_message", @status_message.id, cookies[:socket_id])
      PushPosts.perform_later("status_message", @status_message.id, nil, {lat: location.latitude, lng: location.longitude})
      respond_to do |format|
        format.js
      end      
      mentions_notification_and_push
    end    
  end

  def update
    
  end

private
  def find_postable
    resource, id = URI(request.referrer).path.split('/')[1,2]
    klass = [Event].detect {|c| c.name.underscore.pluralize == resource}
    @postable = klass.friendly.find(id) if klass
  end

  def status_params
    params.require(:status_message).permit(:body).merge(postable: @postable ? @postable : nil)
  end

  def oembed_included?
    params[:oembed_url].present?
  end

  def mentions_notification_and_push
    if @status_message.mentions.present?
      @status_message.mentions.each do |m|
        unless m.user == m.mentioner.user
          notification = m.notifications.create(user: m.user, notifier: m.mentioner.user)
          PushNotifications.perform_later(
                "notifications", notification.id, m.user.id, "notification-created"
              )   
        end
      end
    end
  end
end