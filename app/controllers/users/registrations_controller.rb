class Users::RegistrationsController < Devise::RegistrationsController
  def new
    build_resource({})
    @validatable = devise_mapping.validatable?
    if @validatable
      @minimum_password_length = resource_class.password_length.min
    end
    yield resource if block_given?   
    respond_with self.resource
  end
end