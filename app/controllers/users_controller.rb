class UsersController < ApplicationController
  before_filter :authenticate_user!, except: [:show]
  
  def index
  end
  
  def show
    @user = User.confirmed.friendly.find(params[:id].downcase)
    @sightings = @user.sightings.visible.visible_to(current_user).recent.limit(5)
    @pokestops = @user.pokestops.visible.visible_to(current_user).recent.limit(5)
    @gyms = @user.gyms.visible.visible_to(current_user).recent.limit(5)
    @new_post = StatusMessage.new if user_signed_in?
    @posts = @user.posts.unattached.page(params[:main_pagination]).per_page(5)
    prepare_meta_tags title: "#{@user.full_name} (@#{@user.username})",
                      description: "#{@user.profile.snippet}",
                      image: @user.profile.image? ? @user.profile.image_url(:thumb_large) : ActionController::Base.helpers.asset_path("male_user.png"),
                      twitter: {card: "summary"}
  end

  def edit
    @footer_page = true
    @user = current_user
  end

  def update
    @user = current_user
    if params[:user][:password].blank? && params[:user][:password_confirmation].blank?
        user_params.delete(:password)
        user_params.delete(:password_confirmation)
    end       
    if @user.update_with_password(user_params)
      sign_in @user, bypass: true
      flash[:notice] = "Settings have been successfully saved."
      redirect_to root_path
    else
      render :edit
    end
  end

private
  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, 
      :current_password, profile_attributes: [:id, :first_name, :last_name])
  end
end
