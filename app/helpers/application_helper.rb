module ApplicationHelper
  def google_analytics_js
    content_tag(:script, :type => 'text/javascript') do
      "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-47994362-4', 'auto');
      ga('send', 'pageview');".html_safe
    end if Rails.env.production?
  end

  def poly_path_helper(object, symbol)
    # refactor this into a god damn polymorphic_path
    # this can be deleted once implemented derp
    case object.class.base_class.to_s
    when "Post"; [object.becomes(Post), symbol]
    else; [object, symbol]
    end      
  end

  def flash_class(level)
    case level.to_sym
      when :notice then "alert alert-success"
      when :info then "alert alert-info"
      when :alert then "alert alert-danger"
      when :warning then "alert alert-warning"
    end
  end

  def js_signup_dialog
    return "js-open-signup" unless user_signed_in?
  end
end