class GooglePlaceDetails < ActiveJob::Base
  queue_as :high_priority

  def perform(place_id)
    if place = Place.find(place_id)
      client = GooglePlaces::Client.new(Rails.application.secrets.google_places['key'])
      spot = client.spot(place.google_place_id)
      place.update(
        name: spot.name,
        slug: nil,
        place_type: spot.types[0],
        formatted_address: spot.formatted_address,
        formatted_phone_number: spot.formatted_phone_number,
        rating: spot.rating,
        website: spot.website,
        street_number: spot.street_number,
        route: spot.street,
        city: spot.city,
        state: spot.region,
        country: spot.country,
        postal_code: spot.postal_code,
        longitude: spot.lng,
        latitude: spot.lat,
        remote_image_url: spot.photos.present? ? spot.photos[0].fetch_url(800) : nil
      )
    end
  end
end