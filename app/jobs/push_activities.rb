class PushActivities < ActiveJob::Base
  queue_as :default

  def perform(activity_id, user_id, nearby = false, loc = {}) 
    if activity = PublicActivity::Activity.find(activity_id)
      user = User.find(user_id)

      slugs = user.friends.map {|s| s.slug}
      slugs = slugs << user.slug

      if nearby
        profiles = Profile.joins(:location).near([loc[:lat], loc[:lng]])
        nearby_slugs = profiles.map {|s| s.user.slug}
        slugs = slugs + nearby_slugs
        slugs = slugs - [user.slug]
        slugs.uniq!
      end

      user_channels = slugs.map {|s| "private-user-#{s}"}

      user_channels = user_channels.each_slice(10).to_a
      user_channels.each do |channels|
        Pusher.trigger(channels, 'latest-update', {id: activity_id})
      end
    end
  end
end