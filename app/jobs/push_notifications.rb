class PushNotifications < ActiveJob::Base
  queue_as :default

  def perform(pusher_type, object_id, user_id, trigger) 
    if pusher_type == "conversations"
      klass = Mailboxer::Conversation
    else
      klass = pusher_type.classify.safe_constantize
    end
    
    if object = klass.find(object_id)
      user = User.find(user_id)
      Pusher["private-user-#{user.slug}"].trigger(trigger, {id: object_id, type: pusher_type})
    end
  end
end