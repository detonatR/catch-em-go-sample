class PushPosts < ActiveJob::Base
  queue_as :default

  def perform(pusher_type, object_id, socket_id, loc = {}) 
    if object = Post.find(object_id)
      if loc.empty?
        Pusher.trigger('private-main', 'post-created', {id: object_id, type: pusher_type}, {socket_id: socket_id})
      else
        profiles = Profile.joins(:location).near([loc[:lat], loc[:lng]])
        slugs = profiles.map {|s| s.user.slug}
        slugs = slugs - [object.user.slug]
        user_channels = slugs.map {|s| "private-user-#{s}"}
        user_channels = user_channels.each_slice(10).to_a
        user_channels.each do |channels|
          Pusher.trigger(channels, 'nearby-post-created', {id: object_id, type: pusher_type})
        end
      end
    end
  end
end