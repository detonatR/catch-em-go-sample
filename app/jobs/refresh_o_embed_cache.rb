class RefreshOEmbedCache < ActiveJob::Base
  queue_as :low_priority

  def perform 
    OEmbedCache.find_each do |cache|
      if cache.cache_age <= Time.now.utc
        cache.fetch_and_save_oembed_data!
      end
    end
  end
end