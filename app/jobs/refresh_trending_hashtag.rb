class RefreshTrendingHashtag < ActiveJob::Base
  queue_as :low_priority
  
  def perform
    Hashtag.find_each do |hashtag|
      posts = hashtag.hashtaggings.where(hashtaggable_type: "Post")
      unless posts.present?
        hashtag.update(posts_count: 0, last_post_id: 0, last_post_at: nil)
      else
        last_post = posts.last.hashtaggable
        post_id = last_post.id
        post_at = last_post.created_at
        posts_count = posts.length
        hashtag.update(posts_count: posts_count, last_post_id: post_id, last_post_at: post_at)
      end
    end
  end
end