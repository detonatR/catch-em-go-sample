class SubscribeMailchimp < ActiveJob::Base
  queue_as :low_priority

  def perform(user_id)
    user = User.find(user_id)
    list_id = Rails.application.secrets[:mailchimp]['list']
    Gibbon::Request.api_key = Rails.application.secrets[:mailchimp]['key']
    Gibbon::Request.timeout = 15
    Gibbon::Request.throws_exceptions = false    
    Gibbon::Request.lists(list_id).members.create(body: {
      email_address: user.email,
      status: "subscribed",
      merge_fields: {FNAME: user.profile.first_name, LNAME: user.profile.last_name}
    })
  end
end