class AccountMailer < Devise::Mailer
  include Devise::Controllers::UrlHelpers

  def confirmation_instructions(record, token, opts={})
    @user = record
    @token = token
    html_output = render_to_string template: "devise/mailer/confirmation_instructions"
    message = Mailgun::MessageBuilder.new()
    message.from "no-reply@catchemgo.com", {"first" => "Catch Em", "last" => "Go"}
    message.add_recipient :to, @user.email, {"first" => @user.profile.first_name, "last" => @user.profile.last_name}
    message.subject "Confirm Your Catch 'Em Go Account"
    message.body_html html_output.to_str
    message.add_tag "registration"
    sending = MAILGUN.send_message Rails.application.secrets[:mailgun]['domain'], message
  end

  def reset_password_instructions(record, token, opts={})
    @user = record
    @token = token
    html_output = render_to_string template: "devise/mailer/reset_password_instructions"
    message = Mailgun::MessageBuilder.new()
    message.from "no-reply@catchemgo.com", {"first" => "Catch Em", "last" => "Go"}
    message.add_recipient :to, @user.email, {"first" => @user.profile.first_name, "last" => @user.profile.last_name}
    message.subject "Your Catch 'Em Go Password"
    message.body_html html_output.to_str
    message.add_tag "forgot-password"
    sending = MAILGUN.send_message Rails.application.secrets[:mailgun]['domain'], message    
  end

  def unlock_instructions(record, token, opts={})
    @user = record
    @token = token
    html_output = render_to_string template: "devise/mailer/unlock_instructions"
    message = Mailgun::MessageBuilder.new()
    message.from "no-reply@catchemgo.com", {"first" => "Catch Em", "last" => "Go"}
    message.add_recipient :to, @user.email, {"first" => @user.profile.first_name, "last" => @user.profile.last_name}
    message.subject "Your Catch 'Em Go Account"
    message.body_html html_output.to_str
    message.add_tag "unlock-account"
    sending = MAILGUN.send_message Rails.application.secrets[:mailgun]['domain'], message    
  end
end