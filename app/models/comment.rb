class Comment < ActiveRecord::Base
  include Mentioner
  include PublicActivity::Common

  has_many :likes, as: :likeable
  has_many :notifications, as: :notifiable, dependent: :destroy
  has_many :activities, as: :trackable, class_name: PublicActivity::Activity, dependent: :destroy  
  belongs_to :commentable, polymorphic: true, counter_cache: true
  belongs_to :user

  validates :body, presence: true, length: {:maximum => 65535}

  default_scope { order('id DESC') }
end
