module Hideable
  extend ActiveSupport::Concern
  included do
    scope :visible, -> {where("hidden_at is NULL")}
    scope :hidden, -> {where("hidden_at IS NOT NULL")}
  end

  def hidden?
    self.hidden_at.is_a?(DateTime)
  end
  
  def hide!
    return if self.hidden?
    self.hidden_at = DateTime.now
    self.save!
    if activity = activities.first
      Pusher["private-main"].trigger('latest-update-deleted', {id: activity.id})
      activities.destroy_all
    end
  end

  def unhide!
    return unless self.hidden?
    self.hidden_at = nil
    self.save!
  end

  module ClassMethods
    def visible_to(user)
      #refactor
      return all if user.nil?
      reports = user.reports.where(reportable_type: self.to_s)
      ids = reports.map {|r| r.reportable_id}
      where.not(id: ids)
    end
  end  
end