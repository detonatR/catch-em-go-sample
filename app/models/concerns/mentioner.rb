module Mentioner
  extend ActiveSupport::Concern
  include Twitter::Extractor

  included do
    has_many :mentions, as: :mentioner, dependent: :destroy
    after_save :parse_mentions
  end
  
  def parse_mentions
    mentioned_names = extract_mentioned_screen_names(body)
    mentioned_names.each do |username|
      user = User.find_by(username: username)
      if user
        self.mentions.create!(user: user)
      end
    end
  end
end