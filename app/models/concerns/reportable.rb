module Reportable
  extend ActiveSupport::Concern
  included do 
    has_many :reports, as: :reportable, dependent: :destroy
  end

  def reported_enough?
    self.reports.count >= 5
  end

  def hide_if_reported_enough
    return unless self.reported_enough?
    self.hide!
  end
end