class Event < ActiveRecord::Base
  extend FriendlyId
  attr_accessor :start_date, :start_time, :end_date, :end_time

  reverse_geocoded_by "places.latitude", "places.longitude"

  belongs_to :host, polymorphic: true
  has_many :posts, as: :postable, dependent: :destroy
  has_many :photos, as: :photoable, dependent: :destroy
  has_many :status_messages, as: :postable
  has_many :rsvps, as: :rsvpable, dependent: :destroy
  has_many :users, through: :rsvps
  belongs_to :place
  
  scope :published, -> { where(hidden: false) }
  scope :hidden, -> { where(hidden: true) }
  scope :recent, -> { order("start ASC") }
  scope :upcoming, lambda { where("start >= ?", 1.hour.ago)}
  scope :passed, lambda { where("end >= ?", 1.hour.ago)}
  scope :popular, -> {order("ranking(events.id, rsvps_count + posts_count, 3) DESC")}
  scope :featured, -> {where(featured: true)}

  friendly_id :name, use: :slugged
  mount_uploader :image, EventUploader, mounted_on: :image
  validates :name, :start, presence: true

  def refresh_rsvp_counters
    Event.update(id,
                 rsvps_count: rsvps.length,
                 goings_count: rsvps.where(status: "yes").length,
                 maybes_count: rsvps.where(status: "maybe").length,
                 declines_count: rsvps.where(status: "no").length)
  end

end
