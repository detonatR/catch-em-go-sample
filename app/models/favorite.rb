class Favorite < ActiveRecord::Base
  include PublicActivity::Common
    
  has_many :notifications, as: :notifiable, dependent: :destroy
  has_many :activities, as: :trackable, class_name: PublicActivity::Activity, dependent: :destroy

  belongs_to :user
  belongs_to :favoritable, polymorphic: true
  
  validates :user_id, uniqueness: {scope: [:favoritable_id, :favoritable_type]}

  default_scope { order('id DESC') }
end
