class Like < ActiveRecord::Base
  include PublicActivity::Common

  has_many :notifications, as: :notifiable, dependent: :destroy
  has_many :activities, as: :trackable, class_name: PublicActivity::Activity, dependent: :destroy

  belongs_to :user
  belongs_to :likeable, polymorphic: true, counter_cache: true

  validates :user_id, uniqueness: {scope: [:likeable_id, :likeable_type]}  

end
