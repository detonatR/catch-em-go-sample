class Location < ActiveRecord::Base
  geocoded_by :formatted_address

  has_many :profiles
  has_many :users, through: :profiles
  has_many :posts
  has_many :status_messages

  validates :country, :formatted_address, :latitude, :longitude, presence: true
end
