class Marker < ActiveRecord::Base
  belongs_to :markable, polymorphic: true
  belongs_to :place

  validates :latitude, uniqueness: {scope: [:longitude]}

  def set_place(google_id)
    self.place = Place.find_or_create_by(google_place_id: google_id)
    GooglePlaceDetails.perform_later(self.place.id)
    self.save!
  end
end
