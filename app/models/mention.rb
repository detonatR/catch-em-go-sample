class Mention < ActiveRecord::Base
  has_many :notifications, as: :notifiable, dependent: :destroy
  belongs_to :user
  belongs_to :mentioner, polymorphic: true
  before_destroy :remove_pusher, prepend: true

private
  def remove_pusher
    unless user == mentioner.user
      notification_id = notifications.first.id
      Pusher.trigger("private-user-#{user.slug}", 'notification-deleted', {id: notification_id, type: "notification"})
    end
  end
end
