class NewsFeed < ActiveRecord::Base
  mount_uploader :image, NewsUploader, mounted_on: :image
  default_scope {order('id DESC')}
end
