class Notification < ActiveRecord::Base
  belongs_to :user
  belongs_to :notifiable, polymorphic: true
  belongs_to :notifier, polymorphic: true

  scope :unread, -> { where(read: false) }

  default_scope { order("created_at DESC") }

end
