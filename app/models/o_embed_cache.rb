class OEmbedCache < ActiveRecord::Base
  serialize :data
  validates :url, :data, presence: true
  validates :url, uniqueness: {case_sensitive: false}

  has_many :posts

  def self.find_or_create_by(url)
    cache = OEmbedCache.find_or_initialize_by(url: url)
    return cache if cache.persisted?
    cache.fetch_and_save_oembed_data!
    cache
  end

  def fetch_and_save_oembed_data!
    begin
      client = Embedly::API.new key: Rails.application.secrets[:embedly]['key']
      embedly = client.oembed url: self.url
    rescue => e
      # nothing
    else
      self.data = embedly.first.marshal_dump
      self.cache_age = Time.now.utc + 24.hours
      self.save
    end
  end
end
