class Place < ActiveRecord::Base
  extend FriendlyId
  friendly_id :slug_candidates, use: :slugged

  mount_uploader :image, EventUploader, mounted_on: :image    
  has_many :events
  has_many :markers
  
  geocoded_by :formatted_address

  def slug_candidates
    [
      :name,
      [:name, :city],
      [:name, :street_number, :city]
    ]
  end
end
