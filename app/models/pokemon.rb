class Pokemon < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged
  has_many :pokemon_moves, dependent: :destroy  
  has_many :moves, through: :pokemon_moves
  has_many :sightings
  mount_uploader :image, PokemonUploader, mounted_on: :image   

  scope :non_legendary, -> {where.not(name: ["Mewtwo", "Mew", "Zapdos", "Moltres", "Articuno", "Ditto"])} 
end
