class PokemonMove < ActiveRecord::Base
  belongs_to :pokemon
  belongs_to :move

  validates :pokemon, presence: true
  validates :move, presence: true, uniqueness: { scope: :pokemon }
end
