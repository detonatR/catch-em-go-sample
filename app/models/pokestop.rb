class Pokestop < ActiveRecord::Base
  include PublicActivity::Common
  include Hideable
  include Reportable
  reverse_geocoded_by "markers.latitude", "markers.longitude"

  extend FriendlyId
  friendly_id :name, use: :slugged   
  has_many :activities, as: :trackable, class_name: PublicActivity::Activity, dependent: :destroy
  has_one :marker, as: :markable, dependent: :destroy  
  accepts_nested_attributes_for :marker
  belongs_to :user

  scope :recent, -> {order("created_at DESC")}

  validates :user, :name, :marker, presence: true
end
