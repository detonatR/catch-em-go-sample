class Post < ActiveRecord::Base
  include Hashtaggable
  include Mentioner
  has_many :likes, as: :likeable, dependent: :destroy
  has_many :comments, as: :commentable, dependent: :destroy
  has_many :shares, as: :shareable, dependent: :destroy
  # turn into query since this shit dont work:
  # has_many :sharers, through: :shares, source: :poster, source_type: :poster_type
  has_many :favorites, as: :favoritable, dependent: :destroy
  belongs_to :location
  belongs_to :user
  belongs_to :postable, polymorphic: true, counter_cache: true
  belongs_to :o_embed_cache
  
  default_scope { order('id DESC') }
  scope :unattached, -> { where(postable_id: nil) }

  validates_length_of :body, maximum: 65535, message: proc {|p, v| "Please make your post fewer than 65535 characters. Right now it is #{v[:value].length} characters" }
end
