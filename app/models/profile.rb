class Profile < ActiveRecord::Base
  belongs_to :user
  belongs_to :location
  belongs_to :team
  
  reverse_geocoded_by "locations.latitude", "locations.longitude"

  mount_uploader :image, PhotoUploader, mounted_on: :image

  after_commit :reindex_user

  validates :first_name, presence: true, length: {maximum: 32}

  accepts_nested_attributes_for :location

  def reindex_user
    user.reindex
  end

end
