class Report < ActiveRecord::Base
  belongs_to :user
  belongs_to :reportable, polymorphic: true
  validates :user_id, uniqueness: {scope: [:reportable_id, :reportable_type]}

  after_save :hide_reportable

private

  def hide_reportable
    reportable.hide_if_reported_enough
  end

end
