class Rsvp < ActiveRecord::Base
  include PublicActivity::Common

  STATUSES = ["yes", "maybe", "no"]

  has_many :notifications, as: :notifiable, dependent: :destroy
  has_many :activities, as: :trackable, class_name: PublicActivity::Activity, dependent: :destroy  
  belongs_to :rsvpable, polymorphic: true, counter_cache: true
  belongs_to :user

  validates :user_id, uniqueness: {scope: [:rsvpable_id, :rsvpable_type]}
  validates :status, inclusion: {in: Rsvp::STATUSES}

  after_save :update_custom_counters

private

  def update_custom_counters
    rsvpable.refresh_rsvp_counters
  end

end
