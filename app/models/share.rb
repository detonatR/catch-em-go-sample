class Share < Post
  has_many :notifications, as: :notifiable, dependent: :destroy
  belongs_to :shareable, polymorphic: true, counter_cache: true
  delegate :user, to: :shareable, prefix: true
  delegate :o_embed_cache, to: :absolute_root, allow_nil: true

  def absolute_root
    @absolute_root ||= self 
    @absolute_root = @absolute_root.shareable while @absolute_root.is_a? Share
    @absolute_root
  end  
end