class Sighting < ActiveRecord::Base
  include PublicActivity::Common  
  include Hideable
  include Reportable
  reverse_geocoded_by "markers.latitude", "markers.longitude"
  attr_accessor :seen_date, :seen_time
  has_many :activities, as: :trackable, class_name: PublicActivity::Activity, dependent: :destroy
  has_one :marker, as: :markable, dependent: :destroy
  accepts_nested_attributes_for :marker
  belongs_to :user
  belongs_to :pokemon

  after_create :set_expires_at

  validates :user, :pokemon, :marker, :seen_at, presence: true

  scope :expired, -> {where("expires_at <= ?", Time.now)}
  scope :active, -> {where("expires_at > ?", Time.now)}
  scope :recent, -> {order("seen_at DESC")}

private
  def set_expires_at
    self.expires_at = seen_at + 15.minutes
    self.save
  end
end
