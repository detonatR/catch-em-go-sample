class StatusMessage < Post
  has_one :photo, as: :photoable, dependent: :destroy

  def attach_oembed_data(oembed_url)
    self.o_embed_cache = OEmbedCache.find_by(url: oembed_url)
  end
end