class Team < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged  
  mount_uploader :image, TeamUploader, mounted_on: :image  
  BLACKLIST = %w(admin administrator administer)
  name_regex = /\A([a-zA-Z](_?[a-zA-Z0-9]+)*_?|_([a-zA-Z0-9]+_?)*)\z/i  
  validates :name, presence: true, 
                   uniqueness: {case_sensitive: false}, 
                   format: {with: name_regex, message: "should only contain letters or numbers"},
                   length: {within: 1..20},
                   exclusion: {in: Team::BLACKLIST, message: "is reserved."}

  has_many :profiles
  has_many :users, through: :profiles
  
end
