class User < ActiveRecord::Base
  include Mentionable
  extend FriendlyId
  searchkick autocomplete: ['username', 'full_name']
  acts_as_messageable
  friendly_id :username, use: :slugged  

  devise :database_authenticatable, :registerable, :rememberable,
         :recoverable, :trackable, :validatable, :confirmable, :lockable, :timeoutable

  BLACKLIST = %w(admin administrator administer)

  username_regex = /\A([a-zA-Z](_?[a-zA-Z0-9]+)*_?|_([a-zA-Z0-9]+_?)*)\z/i  
  validates :username, presence: true, 
                       uniqueness: {case_sensitive: false}, 
                       format: {with: username_regex, message: "should only contain letters or numbers"},
                       length: {within: 1..20},
                       exclusion: {in: User::BLACKLIST, message: "is reserved."}
  has_many :sightings
  has_many :gyms
  has_many :pokestops
  has_many :friend_requests, dependent: :destroy
  has_many :pending_friends, through: :friend_requests, source: :friend
  has_many :friendships, dependent: :destroy
  has_many :friends, through: :friendships
  has_many :photos, dependent: :destroy
  has_many :posts, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :status_messages
  has_many :favorites, dependent: :destroy
  has_many :likes, dependent: :destroy
  has_many :notifications, dependent: :destroy
  has_many :sent_notifications, as: :notifier, class_name: "Notification", dependent: :destroy
  has_many :events, as: :host, dependent: :destroy
  has_many :rsvps, dependent: :destroy
  has_many :rsvp_events, through: :rsvps, source: :rsvpable, source_type: "Event"
  has_one :profile, dependent: :destroy
  has_many :reports, dependent: :destroy
  accepts_nested_attributes_for :profile

  has_many :shares, dependent: :destroy, class_name: "Share"
  # shares scope
  scope :confirmed, lambda { where("confirmed_at IS NOT NULL")}

  default_scope { order('id DESC') }

  after_create :subscribe_user_to_mailing_list

  def send_devise_notification(notification, *args)
    devise_mailer.send(notification, self, *args).deliver_later
  end

  def full_name
    "#{profile.first_name} #{profile.last_name}"
  end

  def feed
    Post.unattached
  end

  def mailboxer_email(object)
    email
  end

  def friend_requested?(friend)
    friend_requests.exists?(friend: friend)
  end

  def remove_friend(friend)
    friends.destroy(friend)    
  end

  def likes?(object_id, object_type)
    likes.exists?(likeable_id: object_id, likeable_type: object_type)
  end

  def favorites?(object_id, object_type)
    favorites.exists?(favoritable_id: object_id, favoritable_type: object_type)
  end

  def shares?(object_id, object_type)
    shares.exists?(shareable_id: object_id, shareable_type: object_type)
  end  

  def rsvp_to!(rsvpable, status)
    rsvps.create!(rsvpable: rsvpable, status: status)
  end

  def rsvped_to?(rsvpable_id, rsvpable_type)
    rsvps.where(rsvpable_id: rsvpable_id, rsvpable_type: rsvpable_type).exists?
  end

  def should_index?
    confirmed?
  end

  def search_data
    {
      username: username, 
      full_name: full_name
    }
  end

private

  def subscribe_user_to_mailing_list
    SubscribeMailchimp.perform_later(self.id) if Rails.env.production?
  end

end
