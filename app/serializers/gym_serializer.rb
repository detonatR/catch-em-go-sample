class GymSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  attributes :id, :name, :user, :lat, :lng, :image_url, :report_url

  def user
    object.user.username
  end

  def lat
    object.marker.latitude
  end

  def lng
    object.marker.longitude
  end

  def image_url
    ActionController::Base.helpers.asset_path("map/map_gym.png")
  end

  def report_url
    if scope
      "<a href='#{gym_reports_path(object)}' data-confirm='Are you sure you want to report this?' data-method='post' data-remote='true'>Report</a>"
    else
      "<a href='#' class='js-open-signup'>Report</a>"
    end
  end
end
