class SightingSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  attributes :id, :pokedex, :name, :user, :lat, :lng, :seen_at, :expires_at, :image_url, :report_url

  def pokedex
    object.pokemon.pokedex
  end

  def name
    object.pokemon.name
  end

  def user
    object.user.username
  end

  def lat
    object.marker.latitude
  end

  def lng
    object.marker.longitude
  end

  def expires_at
    object.expires_at.to_i
  end

  def image_url
    object.pokemon.image_url(:icon)
  end

  def report_url
    if scope
      "<a href='#{sighting_reports_path(object)}' data-confirm='Are you sure you want to report this?' data-method='post' data-remote='true'>Report</a>"
    else
      "<a href='#' class='js-open-signup'>Report</a>"
    end
  end
end
