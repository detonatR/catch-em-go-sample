class ApplicationUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  include CarrierWave::MimeTypes
  process :set_content_type
  before :cache, :reset_secure_token

  def filename
    "#{secure_token(10)}.#{file.extension}" if original_filename.present?
  end  

  def extension_white_list
    %w(jpg jpeg png tiff)
  end  

protected

  def secure_token(length)
    model.random_string ||= SecureRandom.hex(length)
  end

  def reset_secure_token(file)
    model.random_string = nil
  end  
  
end
