class EventUploader < ApplicationUploader

  version :thumb do
    process :resize_to_fill => [149,138]
  end

  version :sidebar do
    process :resize_to_fill => [280,114]
  end

  version :full do
    process :resize_to_fill => [1140,302]
  end

  def store_dir
    "images"
  end

end
