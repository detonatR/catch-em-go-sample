class NewsUploader < ApplicationUploader

  def store_dir
    "news"
  end

  version :thumb_medium do
    process :resize_to_fill => [280,114]
  end

end
