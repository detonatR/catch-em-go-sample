class PhotoUploader < ApplicationUploader

  def store_dir
    "images"
  end

  # SMALL
  # messages 36x36
  # message 46x46
  # feed 59x52
  # nav 29x29
  # comments 32x2
  # suggested 38x35
  # latestupdates 25x22

  # MEDIUM 
  # search 118x104

  # LARGE 
  # profile image 232x205
  # photos 186x148

  version :thumb_small do
    process :resize_to_fill => [50,50]
  end

  version :thumb_medium do
    process :resize_to_fill => [100,100]
  end

  version :thumb_large do
    process :resize_to_fill => [300,300]
  end

  version :scaled_full do
    process :resize_to_limit => [700,nil]
    process :get_version_dimensions
  end

  def get_version_dimensions
    if model.has_attribute?(:width) && model.has_attribute?(:height)
      model.width, model.height = `identify -format "%wx%h " #{file.path}`.split(/x/)
    end
  end  

end