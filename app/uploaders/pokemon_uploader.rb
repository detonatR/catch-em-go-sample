class PokemonUploader < ApplicationUploader

  def store_dir
    "pokemon"
  end

  version :icon do
    process :resize_to_fill => [48,48]
  end  

end
