class TeamUploader < ApplicationUploader

  def store_dir
    "teams"
  end

  version :flair do 
    process :resize_to_fill => [25, 25]
  end

  version :thumb_small do
    process :resize_to_fill => [50,50]
  end

  version :thumb_medium do
    process :resize_to_fill => [100,100]
  end

  version :scaled_full do
    process :resize_to_limit => [700,nil]
    process :get_version_dimensions
  end

  def get_version_dimensions
    if model.has_attribute?(:width) && model.has_attribute?(:height)
      model.width, model.height = `identify -format "%wx%h " #{file.path}`.split(/x/)
    end
  end  
  
end