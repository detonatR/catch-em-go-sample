# config valid only for Capistrano 3.1
lock '3.4.1'

set :application, 'catchemgo'
set :repo_url, 'git@bitbucket.org:aoeinteractive/catch-em-go-site.git'
set :deploy_to, '/home/deployer/app/'
set :scm, :git
set :linked_files, %w{config/database.yml}
set :linked_dirs, %w{log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}
set :bundle_binstubs, nil
set :default_env, { path: "~/.rbenv/shims:~/.rbenv/bin:$PATH" }
set :keep_releases, 5
# setup rbenv.
set :rbenv_type, :user
set :rbenv_ruby, '2.3.0'
set :rbenv_map_bins, %w{rake gem bundle ruby rails sidekiq sidekiqctl}

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:all), in: :sequence, wait: 5 do
      invoke 'puma:restart'
    end
    on roles(:worker), in: :sequence, wait: 5 do
      invoke 'clockwork:restart'
    end
  end

  after :publishing, :restart
  after :published, 'nginx:reload'

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

end
