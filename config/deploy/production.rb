set :stage, :production
set :rails_env, "production"
set :app_env, "#{fetch(:application)}_#{fetch(:rails_env)}"
set :user, 'deployer'
set :branch, 'master'
set :puma_threads, [0,5]
set :puma_workers, 0
set :puma_bind, "unix://#{shared_path}/tmp/sockets/puma.sock"
set :puma_state, "#{shared_path}/tmp/pids/puma.state"
set :puma_pid, "#{shared_path}/tmp/pids/puma.pid"
set :puma_access_log, "#{release_path}/log/puma.error.log"
set :puma_error_log,  "#{release_path}/log/puma.access.log"
set :puma_preload_app, true
set :puma_worker_timeout, nil
set :puma_role, :all
set :puma_init_active_record, true

# set :sidekiq_config, -> { File.join(current_path, 'config', 'sidekiq.yml') }
# set :sidekiq_options, "-q mailers -q carrierwave -q default -q low_priority -q high_priority"
# set :sidekiq_role, :all

server '52.43.163.33', user: "#{fetch(:user)}", roles: %w{web app db worker}, primary: true
server '52.37.112.98', user: "#{fetch(:user)}", roles: %{web app}