require 'fog/aws/storage'
require 'carrierwave'

CarrierWave.configure do |config|
  if Rails.env.development? || Rails.env.test?
    config.storage = :file
  else
    config.storage = :fog
    config.fog_credentials = {
      provider: "AWS",
      aws_access_key_id: Rails.application.secrets[:aws]['key'],
      aws_secret_access_key: Rails.application.secrets[:aws]['secret'],
      path_style: true
    }
    config.fog_directory = Rails.application.secrets[:carrierwave]['bucket']
    config.fog_attributes = {'Cache-Control'=>'max-age=2678400'}
    config.asset_host = Rails.application.secrets[:carrierwave]['host']
  end
end