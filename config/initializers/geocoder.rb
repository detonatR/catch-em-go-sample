redis = Redis.connect(url: Rails.application.secrets[:sidekiq]['url'])

Geocoder.configure(
  timeout: 30,
  lookup: :bing,
  bing: {
    api_key: Rails.application.secrets[:bing]['key']
  },
  cache: redis,
  always_raise: [
    Geocoder::OverQueryLimitError,
    Geocoder::RequestDenied,
    Geocoder::InvalidRequest,
    Geocoder::InvalidApiKey
  ],  
  ip_lookup: :maxmind_local,
  maxmind_local: {package: :city}
)
