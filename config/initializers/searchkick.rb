require 'typhoeus/adapters/faraday'
require 'faraday_middleware/aws_signers_v4'

class AmazonElasticSearchClient
  def self.client
    return Elasticsearch::Client.new url: Rails.application.secrets[:searchkick]['url'], transport_options: {request: {timeout: 10}} do |f|
      f.request :aws_signers_v4, {
                credentials: Aws::Credentials.new(Rails.application.secrets[:aws]['key'], Rails.application.secrets[:aws]['secret']),
                service_name: 'es',
                region: 'us-west-2'
      }
    end
  end
end

Searchkick.client = AmazonElasticSearchClient.client if Rails.env.production?