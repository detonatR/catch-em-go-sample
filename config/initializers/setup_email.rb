ActionMailer::Base.smtp_settings = {
  :address        => "smtp.mailgun.org",
  :port           => 587,
  :user_name      => Rails.application.secrets[:mailgun]['username'],
  :password       => Rails.application.secrets[:mailgun]['password'],
}

MAILGUN = Mailgun::Client.new Rails.application.secrets[:mailgun]['key']