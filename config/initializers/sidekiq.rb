if Rails.env.production? || Rails.env.staging?
  Sidekiq.configure_server do |config|
    config.redis = { url: Rails.application.secrets[:sidekiq]['url'], network_timeout: 5 }
  end

  Sidekiq.configure_client do |config|
    config.redis = { url: Rails.application.secrets[:sidekiq]['url'], network_timeout: 5 }
  end  
end