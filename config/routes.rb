Rails.application.routes.draw do
  require 'sidekiq/web'

  get 'home', to: 'home#index'

  get 'mentions/users',           to: 'mentions#users'
  get 'latest-updates',           to: 'activities#latest_updates', 
                                  as: :latest_updates
  get 'about',                    to: 'static#about'
  get 'rsvps/paginate',           to: 'rsvps#paginate'
  get 'notifications/paginate',   to: 'notifications#paginate'
  get 'friend_requests/paginate', to: 'friend_requests#paginate'
  get 'messages/paginate',        to: 'conversations#paginate'
  get 'contact',                  to: 'static#contact'
  get 'privacy',                  to: 'static#privacy'
  get 'tos',                      to: 'static#tos'
  get 'search',                   to: 'search#index'
  get 'search/users',             to: 'search#users'
  get 'search/hashtags',          to: 'search#hashtags'
  get 'messages',                 to: 'conversations#index'
  get 'notifications/dropdown',   to: 'notifications#dropdown'
  get 'messages/dropdown',        to: 'conversations#dropdown'
  get 'friend_requests/dropdown', to: 'friend_requests#dropdown'
  get 'profile/edit',             to: 'profiles#edit'
  get 'profile/edit/header',      to: 'profiles#edit_header'
  get 'nearby/events',            to: 'nearby#events'
  get 'nearby/trainers',          to: 'nearby#trainers'
  get 'nearby/posts',             to: 'nearby#posts'
  post 'embedly',                 to: 'posts#embedly', as: 'embedly'
  post 'pusher/auth'
  get 'pusher/notifications',     to: 'pusher#notifications'
  get 'pusher/feed',              to: 'pusher#feed'
  get 'pusher/updates',           to: 'pusher#updates'
  post 'map/search/:id',          to: 'map#search'
  get 'map/sightings/:lat/:lng',  to: 'map#sightings', lat: /-?\d+(\.\d+)/, lng: /-?\d+(\.\d+)/
  get 'map/gyms/:lat/:lng',       to: 'map#gyms', lat: /-?\d+(\.\d+)/, lng: /-?\d+(\.\d+)/
  get 'map/pokestops/:lat/:lng',  to: 'map#pokestops', lat: /-?\d+(\.\d+)/, lng: /-?\d+(\.\d+)/
  
  get 'change-team',              to: 'change_team#new'
  patch 'change-team',             to: 'change_team#create'

  resources :sightings do
    resources :reports
  end
  resources :gyms do
    resources :reports
  end
  resources :pokestops do
    resources :reports
  end
  
  resources :mentions, only: [:index]
  resources :friend_requests, path: "friends/requests"
  delete 'friends/:id', to: "friends#destroy", as: :friends
  resources :notifications
  resources :activities, only: [:index]
  resources :hashtags, only: [:index, :show]

  resources :events do
    resources :rsvps
    get 'hosting', on: :collection
    get 'rsvps', on: :collection
    put "update_image", to: 'events#update_image'
    delete "remove_image", to: 'events#remove_image'
  end

  resources :users, only: [:index, :show, :update] do
    resources :profiles
    resources :locations
    get 'likes', to: 'users#likes'
    get 'favorites', to: 'profiles#favorites'
    get 'about', to: 'profiles#show'
    get 'photos', to: 'photos#index'
    get 'friends', to: 'friends#index'
    delete 'remove_avatar', to: 'profiles#remove_avatar'
    put 'update_avatar', to: 'profiles#update_avatar'
    put 'update_header', to: 'profiles#update_header'
  end  

  resources :photos do
    resources :comments
    resources :likes, only: [:index, :create]
    resources :locations
    delete 'likes', to: 'likes#destroy'
  end

  resources :status_messages, only: [:new, :create, :update]   
  resources :messages, only: [:new, :create, :show]

  resources :conversations, only: [:index] do
    member do
      post :reply
    end
  end

  resources :posts, only: [:show, :destroy] do
    resources :locations
    resources :comments
    resources :favorites, only: [:index, :create]
    resources :likes, only: [:index, :create]
    resources :shares, only: [:index, :new, :create]
    delete 'likes', to: 'likes#destroy'
    delete 'favorites', to: 'favorites#destroy'
    delete 'shares', to: 'shares#destroy'
  end

  devise_for :users, path: '', skip: [:registrations], path_names: {sign_in: 'login', sign_out: 'logout', password: 'password-reset', confirmation: 'confirm-account'}, controllers: {confirmations: 'devise/confirmations'}

  as :user do
    get 'register', to: 'users/registrations#new', as: 'new_user_registration'
    post 'register', to: 'devise/registrations#create', as: 'user_registration'
    get 'users/cancel', to: 'devise/registrations#cancel', as: 'cancel_user_registration'
    get 'settings', to: 'users#edit', as: 'edit_user_registration'
    put 'settings', to: 'users#update'
  end    

  Sidekiq::Web.use Rack::Auth::Basic do |username, password|
    username == Rails.application.secrets[:sidekiq]['username'] && password == Rails.application.secrets[:sidekiq]['password']
  end  

  mount Sidekiq::Web => '/sidekiq'
  
  authenticated :user do 
    root to: "dashboard#index", as: "authenticated_root"
  end

  root to: "home#index"  
end
