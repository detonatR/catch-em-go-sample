class CreateHashtags < ActiveRecord::Migration
  def change
    create_table :hashtags do |t|
      t.string :name
      t.integer :posts_count, default: 0
      t.string :display
      t.integer :last_post_id, default: 0
      t.datetime :last_post_at

      t.timestamps null: false
    end

    add_index :hashtags, :last_post_id
    add_index :hashtags, :name, unique: true
    add_index :hashtags, :posts_count
  end
end
