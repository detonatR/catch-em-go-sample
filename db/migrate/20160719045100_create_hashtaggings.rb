class CreateHashtaggings < ActiveRecord::Migration
  def change
    create_table :hashtaggings do |t|
      t.integer :hashtag_id
      t.integer :hashtaggable_id
      t.string :hashtaggable_type

      t.timestamps null: false
    end

    add_index :hashtaggings, :hashtag_id
    add_index :hashtaggings, [:hashtaggable_id, :hashtaggable_type]
  end
end
