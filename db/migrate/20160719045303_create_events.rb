class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.integer :host_id
      t.string :host_type
      t.string :image
      t.text :description
      t.datetime :start
      t.datetime :end
      t.boolean :hidden
      t.string :category
      t.string :random_string
      t.string :slug
      t.integer :place_id
      t.integer :rsvps_count, default: 0
      t.integer :goings_count, default: 0
      t.integer :maybes_count, default: 0
      t.integer :declines_count, default: 0
      t.integer :posts_count, default: 0

      t.timestamps null: false
    end

    add_index :events, :slug, unique: true
    add_index :events, [:host_id, :host_type]
    add_index :events, :place_id
    add_index :events, :category
    add_index :events, :hidden
    add_index :events, :rsvps_count
    add_index :events, :goings_count
    add_index :events, :maybes_count
    add_index :events, :declines_count
    add_index :events, :posts_count
  end
end
