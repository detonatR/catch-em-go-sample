class CreateFavorites < ActiveRecord::Migration
  def change
    create_table :favorites do |t|
      t.integer :favoritable_id
      t.string :favoritable_type
      t.integer :user_id

      t.timestamps null: false
    end
    add_index :favorites, :user_id
    add_index :favorites, [:favoritable_id, :favoritable_type]
  end
end
