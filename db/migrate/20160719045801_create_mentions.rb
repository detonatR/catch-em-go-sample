class CreateMentions < ActiveRecord::Migration
  def change
    create_table :mentions do |t|
      t.string :mentioner_type
      t.integer :mentioner_id
      t.integer :user_id

      t.timestamps null: false
    end
    add_index :mentions, :user_id
    add_index :mentions, [:mentioner_id, :mentioner_type]
  end
end
