class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.integer :user_id
      t.boolean :read, default: false
      t.integer :notifiable_id
      t.string :notifiable_type
      t.integer :notifier_id
      t.string :notifier_type

      t.timestamps null: false
    end

    add_index :notifications, :user_id
    add_index :notifications, [:notifiable_id, :notifiable_type]
    add_index :notifications, [:notifier_id, :notifier_type]
    add_index :notifications, :read
  end
end
