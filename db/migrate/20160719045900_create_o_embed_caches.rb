class CreateOEmbedCaches < ActiveRecord::Migration
  def change
    create_table :o_embed_caches do |t|
      t.string :url
      t.text :data
      t.datetime :cache_age

      t.timestamps null: false
    end

    add_index :o_embed_caches, :url
    add_index :o_embed_caches, :cache_age
  end
end
