class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.integer :user_id
      t.integer :photoable_id
      t.string :photoable_type
      t.text :description
      t.string :image
      t.integer :width
      t.integer :height
      t.string :random_string
      t.integer :likes_count, default: 0
      t.integer :comments_count, default: 0

      t.timestamps null: false
    end

    add_index :photos, :user_id
    add_index :photos, [:photoable_id, :photoable_type]
    add_index :photos, :likes_count
    add_index :photos, :comments_count
  end
end
