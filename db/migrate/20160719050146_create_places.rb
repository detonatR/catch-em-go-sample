class CreatePlaces < ActiveRecord::Migration
  def change
    create_table :places do |t|
      t.string :name
      t.string :formatted_address
      t.string :image
      t.string :formatted_phone_number
      t.float :rating
      t.string :website
      t.string :street_number
      t.string :city
      t.string :state
      t.string :country
      t.string :postal_code
      t.float :longitude
      t.float :latitude
      t.string :google_place_id
      t.string :random_string
      t.string :slug
      t.string :route
      t.string :type
      t.string :poke_place_type
      t.timestamps null: false
    end

    add_index :places, :slug, unique: true
    add_index :places, :google_place_id
    add_index :places, :poke_place_type
  end
end
