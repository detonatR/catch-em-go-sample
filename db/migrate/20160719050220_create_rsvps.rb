class CreateRsvps < ActiveRecord::Migration
  def change
    create_table :rsvps do |t|
      t.integer :user_id
      t.integer :rsvpable_id
      t.string :rsvpable_type
      t.string :status

      t.timestamps null: false
    end
    add_index :rsvps, :user_id
    add_index :rsvps, [:rsvpable_id, :rsvpable_type]
    add_index :rsvps, :status
  end
end
