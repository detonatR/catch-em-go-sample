class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.text :body
      t.string :type
      t.integer :shares_count, default: 0
      t.integer :likes_count, default: 0
      t.integer :favorites_count, default: 0
      t.integer :comments_count, default: 0
      t.integer :user_id
      t.integer :shareable_id
      t.string :shareable_type
      t.integer :postable_id
      t.string :postable_type
      t.integer :o_embed_cache_id
      t.integer :sighting_id
      t.integer :photo_id
      t.integer :location_id
      t.timestamps null: false
    end

    add_index :posts, :location_id
    add_index :posts, :user_id
    add_index :posts, [:postable_id, :postable_type]
    add_index :posts, :photo_id
    add_index :posts, :o_embed_cache_id
    add_index :posts, :sighting_id
    add_index :posts, :shares_count
    add_index :posts, :likes_count
    add_index :posts, :favorites_count
    add_index :posts, :comments_count
    add_index :posts, :type
    add_index :posts, [:shareable_id, :shareable_type, :user_id], name: "index_shares_unique", unique: true
    add_index :posts, :shareable_id, name: "index_posts_on_root_id"
  end
end
