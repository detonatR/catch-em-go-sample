class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.integer :user_id
      t.date :birthday
      t.string :gender
      t.text :about
      t.string :first_name
      t.string :last_name
      t.text :snippet
      t.string :image
      t.string :random_string
      t.integer :location_id
      t.integer :team_id

      t.timestamps null: false
    end
    add_index :profiles, :user_id
    add_index :profiles, :team_id
    add_index :profiles, :location_id
  end
end
