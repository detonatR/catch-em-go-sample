class CreateSightings < ActiveRecord::Migration
  def change
    create_table :sightings do |t|
      t.integer :user_id
      t.integer :pokemon_id
      t.integer :location_id
      t.datetime :seen_at
      t.timestamps null: false
    end
    add_index :sightings, :user_id
    add_index :sightings, :pokemon_id
    add_index :sightings, :location_id
  end
end
