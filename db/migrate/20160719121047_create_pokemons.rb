class CreatePokemons < ActiveRecord::Migration
  def change
    create_table :pokemons do |t|
      t.string :name
      t.string :slug
      t.string :image
      t.string :random_string
      t.string :type
      t.float :weight
      t.float :height 
      t.integer :attack
      t.integer :defense
      t.integer :stamina
      t.float :capture_rate
      t.decimal :flee_chance
      t.integer :dodge_interval     
      t.timestamps null: false
    end

    add_index :pokemons, :slug, unique: true
  end
end
