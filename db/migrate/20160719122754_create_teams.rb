class CreateTeams < ActiveRecord::Migration
  def change
    create_table :teams do |t|
      t.string :name
      t.string :slug
      t.string :image
      t.string :random_string      
      t.timestamps null: false
    end

    add_index :teams, :slug, unique: true
  end
end
