class CreatePokemonMoves < ActiveRecord::Migration
  def change
    create_table :pokemon_moves do |t|
      t.references :pokemon, index: true, foreign_key: true
      t.string :name
      t.string :type
      t.integer :power
      t.integer :charge

      t.timestamps null: false
    end
  end
end
