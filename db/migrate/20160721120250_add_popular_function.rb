class AddPopularFunction < ActiveRecord::Migration
  def change
    execute <<-SQL
      create or replace function
        popularity(count integer, weight integer default 3) RETURNS integer AS $$ SELECT count * weight $$ LANGUAGE SQL IMMUTABLE;
    SQL
  end
end
