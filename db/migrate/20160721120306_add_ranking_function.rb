class AddRankingFunction < ActiveRecord::Migration
  def change
    execute <<-SQL
      create or replace function
        ranking(id integer, counts integer, weight integer) RETURNS integer AS $$ SELECT id + popularity(counts, weight) $$ LANGUAGE SQL IMMUTABLE;
    SQL
  end
end
