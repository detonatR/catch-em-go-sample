class CreateNewsFeeds < ActiveRecord::Migration
  def change
    create_table :news_feeds do |t|
      t.string :title
      t.text :description
      t.string :domain
      t.string :full_url
      t.string :image
      t.string :random_string

      t.timestamps null: false
    end
  end
end
