class CreateMoves < ActiveRecord::Migration
  def change
    create_table :moves do |t|
      t.integer :pokemon_id
      t.string :name
      t.integer :power
      t.decimal :energy
      t.decimal :dps
      t.decimal :accuracy
      t.decimal :critical_chance
      t.decimal :duration
      t.string :type
      t.boolean :special
      t.timestamps null: false
    end
  end
end
