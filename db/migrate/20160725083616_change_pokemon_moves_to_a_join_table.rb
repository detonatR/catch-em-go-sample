class ChangePokemonMovesToAJoinTable < ActiveRecord::Migration
  def change
    add_column :pokemon_moves, :move_id, :integer
    add_index :pokemon_moves, :move_id
    remove_column :pokemon_moves,:name
    remove_column :pokemon_moves,:type
    remove_column :pokemon_moves,:power
    remove_column :pokemon_moves,:charge
  end
end
