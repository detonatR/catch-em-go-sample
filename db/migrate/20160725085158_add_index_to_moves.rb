class AddIndexToMoves < ActiveRecord::Migration
  def change
    add_index :moves, :pokemon_id
    add_index :moves, :type
    add_index :moves, :special
  end
end
