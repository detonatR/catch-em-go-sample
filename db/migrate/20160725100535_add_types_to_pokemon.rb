class AddTypesToPokemon < ActiveRecord::Migration
  def change
    remove_column :pokemons, :type
    add_column :pokemons, :type1, :string
    add_column :pokemons, :type2, :string
  end
end
