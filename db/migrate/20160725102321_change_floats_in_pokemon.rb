class ChangeFloatsInPokemon < ActiveRecord::Migration
  def change
    change_column :pokemons, :capture_rate, :decimal
    change_column :pokemons, :weight, :integer
    change_column :pokemons, :height, :integer
    change_column :pokemons, :flee_chance, :decimal
  end
end
