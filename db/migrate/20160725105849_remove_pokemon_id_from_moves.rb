class RemovePokemonIdFromMoves < ActiveRecord::Migration
  def change
    remove_column :moves, :pokemon_id
  end
end
