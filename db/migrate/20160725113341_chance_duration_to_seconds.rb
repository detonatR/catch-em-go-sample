class ChanceDurationToSeconds < ActiveRecord::Migration
  def change
    change_column :moves, :duration, :integer
  end
end
