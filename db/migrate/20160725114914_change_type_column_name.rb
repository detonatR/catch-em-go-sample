class ChangeTypeColumnName < ActiveRecord::Migration
  def change
    rename_column :moves, :type, :move_type
  end
end
