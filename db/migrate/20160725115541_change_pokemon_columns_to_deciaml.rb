class ChangePokemonColumnsToDeciaml < ActiveRecord::Migration
  def change
    change_column :pokemons, :weight, :decimal
    change_column :pokemons, :height, :decimal
  end
end
