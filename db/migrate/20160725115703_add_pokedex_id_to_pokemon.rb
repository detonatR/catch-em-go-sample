class AddPokedexIdToPokemon < ActiveRecord::Migration
  def change
    add_column :pokemons, :pokedex, :string
  end
end
