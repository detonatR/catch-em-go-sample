class AddExpiresAtToSightings < ActiveRecord::Migration
  def change
    add_column :sightings, :expires_at, :datetime
  end
end
