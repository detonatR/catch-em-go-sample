class RemovePokePlaceTypeFromPlaces < ActiveRecord::Migration
  def change
    remove_column :places, :poke_place_type
    rename_column :sightings, :location_id, :place_id
  end
end
