class CreatePokestops < ActiveRecord::Migration
  def change
    create_table :pokestops do |t|
      t.string :name
      t.integer :place_id
      t.timestamps null: false
    end
  end
end
