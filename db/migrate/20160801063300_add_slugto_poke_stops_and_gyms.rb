class AddSlugtoPokeStopsAndGyms < ActiveRecord::Migration
  def change
    add_column :pokestops, :slug, :string
    add_column :gyms, :slug, :string
    add_index :gyms, :slug, unique: true
    add_index :pokestops, :slug, unique: true
  end
end
