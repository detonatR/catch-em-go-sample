class AddVfxNameToMoves < ActiveRecord::Migration
  def change
    add_column :moves, :vfx_name, :string
    add_index :moves, :vfx_name
  end
end
