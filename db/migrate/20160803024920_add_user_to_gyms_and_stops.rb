class AddUserToGymsAndStops < ActiveRecord::Migration
  def change
    add_column :pokestops, :user_id, :integer
    add_column :gyms, :user_id, :integer
    add_index :pokestops, :user_id
    add_index :gyms, :user_id
  end
end
