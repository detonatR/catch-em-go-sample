class AddHiddenAtToMarkers < ActiveRecord::Migration
  def change
    add_column :sightings, :hidden_at, :datetime
    add_column :gyms, :hidden_at, :datetime
    add_column :pokestops, :hidden_at, :datetime
  end
end
