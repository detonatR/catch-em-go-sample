class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.integer :user_id
      t.string :reportable_type
      t.integer :reportable_id
      t.timestamps null: false
    end

    add_index :reports, :user_id
    add_index :reports, [:reportable_type, :reportable_id]
  end
end
