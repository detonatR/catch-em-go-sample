class CreateMarkers < ActiveRecord::Migration
  def change
    create_table :markers do |t|
      t.float :latitude
      t.float :longitude
      t.integer :markable_id
      t.string :markable_type
      t.integer :place_id
      t.timestamps null: false
    end
    add_index :markers, [:markable_id, :markable_type], unique: true
    add_index :markers, [:latitude, :longitude], unique: true
    add_index :markers, :place_id
  end
end
