class RemovePlaceFromMarkers < ActiveRecord::Migration
  def change
    remove_column :gyms, :place_id
    remove_column :pokestops, :place_id
    remove_column :sightings, :place_id

    add_column :gyms, :marker_id, :integer
    add_column :pokestops, :marker_id, :integer
    add_column :sightings, :marker_id, :integer

    add_index :gyms, :marker_id
    add_index :pokestops, :marker_id
    add_index :sightings, :marker_id
  end
end
