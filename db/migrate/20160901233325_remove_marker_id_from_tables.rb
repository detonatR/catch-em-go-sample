class RemoveMarkerIdFromTables < ActiveRecord::Migration
  def change
    remove_column :sightings, :marker_id
    remove_column :gyms, :marker_id
    remove_column :pokestops, :marker_id
  end
end
