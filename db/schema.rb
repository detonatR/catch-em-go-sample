# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160918050712) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "activities", force: :cascade do |t|
    t.integer  "trackable_id"
    t.string   "trackable_type"
    t.integer  "owner_id"
    t.string   "owner_type"
    t.string   "key"
    t.text     "parameters"
    t.integer  "recipient_id"
    t.string   "recipient_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "activities", ["owner_id", "owner_type"], name: "index_activities_on_owner_id_and_owner_type", using: :btree
  add_index "activities", ["recipient_id", "recipient_type"], name: "index_activities_on_recipient_id_and_recipient_type", using: :btree
  add_index "activities", ["trackable_id", "trackable_type"], name: "index_activities_on_trackable_id_and_trackable_type", using: :btree

  create_table "comments", force: :cascade do |t|
    t.string   "commentable_type"
    t.integer  "commentable_id"
    t.text     "body"
    t.integer  "user_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "comments", ["commentable_id", "commentable_type"], name: "index_comments_on_commentable_id_and_commentable_type", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "events", force: :cascade do |t|
    t.string   "name"
    t.integer  "host_id"
    t.string   "host_type"
    t.string   "image"
    t.text     "description"
    t.datetime "start"
    t.datetime "end"
    t.boolean  "hidden"
    t.string   "category"
    t.string   "random_string"
    t.string   "slug"
    t.integer  "place_id"
    t.integer  "rsvps_count",    default: 0
    t.integer  "goings_count",   default: 0
    t.integer  "maybes_count",   default: 0
    t.integer  "declines_count", default: 0
    t.integer  "posts_count",    default: 0
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.boolean  "featured",       default: false
  end

  add_index "events", ["category"], name: "index_events_on_category", using: :btree
  add_index "events", ["declines_count"], name: "index_events_on_declines_count", using: :btree
  add_index "events", ["featured"], name: "index_events_on_featured", using: :btree
  add_index "events", ["goings_count"], name: "index_events_on_goings_count", using: :btree
  add_index "events", ["hidden"], name: "index_events_on_hidden", using: :btree
  add_index "events", ["host_id", "host_type"], name: "index_events_on_host_id_and_host_type", using: :btree
  add_index "events", ["maybes_count"], name: "index_events_on_maybes_count", using: :btree
  add_index "events", ["place_id"], name: "index_events_on_place_id", using: :btree
  add_index "events", ["posts_count"], name: "index_events_on_posts_count", using: :btree
  add_index "events", ["rsvps_count"], name: "index_events_on_rsvps_count", using: :btree
  add_index "events", ["slug"], name: "index_events_on_slug", unique: true, using: :btree

  create_table "favorites", force: :cascade do |t|
    t.integer  "favoritable_id"
    t.string   "favoritable_type"
    t.integer  "user_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "favorites", ["favoritable_id", "favoritable_type"], name: "index_favorites_on_favoritable_id_and_favoritable_type", using: :btree
  add_index "favorites", ["user_id"], name: "index_favorites_on_user_id", using: :btree

  create_table "friend_requests", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "friend_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "friend_requests", ["friend_id"], name: "index_friend_requests_on_friend_id", using: :btree
  add_index "friend_requests", ["user_id"], name: "index_friend_requests_on_user_id", using: :btree

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "friendships", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "friend_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "friendships", ["friend_id"], name: "index_friendships_on_friend_id", using: :btree
  add_index "friendships", ["user_id"], name: "index_friendships_on_user_id", using: :btree

  create_table "genders", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "genders", ["name"], name: "index_genders_on_name", using: :btree

  create_table "gyms", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "slug"
    t.integer  "user_id"
    t.datetime "hidden_at"
  end

  add_index "gyms", ["slug"], name: "index_gyms_on_slug", unique: true, using: :btree
  add_index "gyms", ["user_id"], name: "index_gyms_on_user_id", using: :btree

  create_table "hashtaggings", force: :cascade do |t|
    t.integer  "hashtag_id"
    t.integer  "hashtaggable_id"
    t.string   "hashtaggable_type"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "hashtaggings", ["hashtag_id"], name: "index_hashtaggings_on_hashtag_id", using: :btree
  add_index "hashtaggings", ["hashtaggable_id", "hashtaggable_type"], name: "index_hashtaggings_on_hashtaggable_id_and_hashtaggable_type", using: :btree

  create_table "hashtags", force: :cascade do |t|
    t.string   "name"
    t.integer  "posts_count",  default: 0
    t.string   "display"
    t.integer  "last_post_id", default: 0
    t.datetime "last_post_at"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "hashtags", ["last_post_id"], name: "index_hashtags_on_last_post_id", using: :btree
  add_index "hashtags", ["name"], name: "index_hashtags_on_name", unique: true, using: :btree
  add_index "hashtags", ["posts_count"], name: "index_hashtags_on_posts_count", using: :btree

  create_table "likes", force: :cascade do |t|
    t.string   "likeable_type"
    t.integer  "likeable_id"
    t.integer  "user_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "likes", ["likeable_id", "likeable_type"], name: "index_likes_on_likeable_id_and_likeable_type", using: :btree
  add_index "likes", ["user_id"], name: "index_likes_on_user_id", using: :btree

  create_table "locations", force: :cascade do |t|
    t.string   "state"
    t.string   "city"
    t.string   "country"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "formatted_address"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "mailboxer_conversation_opt_outs", force: :cascade do |t|
    t.integer "unsubscriber_id"
    t.string  "unsubscriber_type"
    t.integer "conversation_id"
  end

  add_index "mailboxer_conversation_opt_outs", ["conversation_id"], name: "index_mailboxer_conversation_opt_outs_on_conversation_id", using: :btree
  add_index "mailboxer_conversation_opt_outs", ["unsubscriber_id", "unsubscriber_type"], name: "index_mailboxer_conversation_opt_outs_on_unsubscriber_id_type", using: :btree

  create_table "mailboxer_conversations", force: :cascade do |t|
    t.string   "subject",    default: ""
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "mailboxer_notifications", force: :cascade do |t|
    t.string   "type"
    t.text     "body"
    t.string   "subject",              default: ""
    t.integer  "sender_id"
    t.string   "sender_type"
    t.integer  "conversation_id"
    t.boolean  "draft",                default: false
    t.string   "notification_code"
    t.integer  "notified_object_id"
    t.string   "notified_object_type"
    t.string   "attachment"
    t.datetime "updated_at",                           null: false
    t.datetime "created_at",                           null: false
    t.boolean  "global",               default: false
    t.datetime "expires"
  end

  add_index "mailboxer_notifications", ["conversation_id"], name: "index_mailboxer_notifications_on_conversation_id", using: :btree
  add_index "mailboxer_notifications", ["notified_object_id", "notified_object_type"], name: "index_mailboxer_notifications_on_notified_object_id_and_type", using: :btree
  add_index "mailboxer_notifications", ["sender_id", "sender_type"], name: "index_mailboxer_notifications_on_sender_id_and_sender_type", using: :btree
  add_index "mailboxer_notifications", ["type"], name: "index_mailboxer_notifications_on_type", using: :btree

  create_table "mailboxer_receipts", force: :cascade do |t|
    t.integer  "receiver_id"
    t.string   "receiver_type"
    t.integer  "notification_id",                            null: false
    t.boolean  "is_read",                    default: false
    t.boolean  "trashed",                    default: false
    t.boolean  "deleted",                    default: false
    t.string   "mailbox_type",    limit: 25
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  add_index "mailboxer_receipts", ["notification_id"], name: "index_mailboxer_receipts_on_notification_id", using: :btree
  add_index "mailboxer_receipts", ["receiver_id", "receiver_type"], name: "index_mailboxer_receipts_on_receiver_id_and_receiver_type", using: :btree

  create_table "markers", force: :cascade do |t|
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "markable_id"
    t.string   "markable_type"
    t.integer  "place_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "markers", ["latitude", "longitude"], name: "index_markers_on_latitude_and_longitude", unique: true, using: :btree
  add_index "markers", ["markable_id", "markable_type"], name: "index_markers_on_markable_id_and_markable_type", unique: true, using: :btree
  add_index "markers", ["place_id"], name: "index_markers_on_place_id", using: :btree

  create_table "maxmind_geolite_city_blocks", id: false, force: :cascade do |t|
    t.integer "start_ip_num", limit: 8, null: false
    t.integer "end_ip_num",   limit: 8, null: false
    t.integer "loc_id",       limit: 8, null: false
  end

  add_index "maxmind_geolite_city_blocks", ["end_ip_num", "start_ip_num"], name: "index_maxmind_geolite_city_blocks_on_end_ip_num_range", unique: true, using: :btree
  add_index "maxmind_geolite_city_blocks", ["loc_id"], name: "index_maxmind_geolite_city_blocks_on_loc_id", using: :btree
  add_index "maxmind_geolite_city_blocks", ["start_ip_num"], name: "index_maxmind_geolite_city_blocks_on_start_ip_num", unique: true, using: :btree

  create_table "maxmind_geolite_city_location", id: false, force: :cascade do |t|
    t.integer "loc_id",      limit: 8, null: false
    t.string  "country",               null: false
    t.string  "region",                null: false
    t.string  "city"
    t.string  "postal_code",           null: false
    t.float   "latitude"
    t.float   "longitude"
    t.integer "metro_code"
    t.integer "area_code"
  end

  add_index "maxmind_geolite_city_location", ["loc_id"], name: "index_maxmind_geolite_city_location_on_loc_id", unique: true, using: :btree

  create_table "mentions", force: :cascade do |t|
    t.string   "mentioner_type"
    t.integer  "mentioner_id"
    t.integer  "user_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "mentions", ["mentioner_id", "mentioner_type"], name: "index_mentions_on_mentioner_id_and_mentioner_type", using: :btree
  add_index "mentions", ["user_id"], name: "index_mentions_on_user_id", using: :btree

  create_table "moves", force: :cascade do |t|
    t.string   "name"
    t.integer  "power"
    t.decimal  "energy"
    t.decimal  "dps"
    t.decimal  "accuracy"
    t.decimal  "critical_chance"
    t.integer  "duration"
    t.string   "move_type"
    t.boolean  "special"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "vfx_name"
  end

  add_index "moves", ["move_type"], name: "index_moves_on_move_type", using: :btree
  add_index "moves", ["special"], name: "index_moves_on_special", using: :btree
  add_index "moves", ["vfx_name"], name: "index_moves_on_vfx_name", using: :btree

  create_table "news_feeds", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.string   "domain"
    t.string   "full_url"
    t.string   "image"
    t.string   "random_string"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "notifications", force: :cascade do |t|
    t.integer  "user_id"
    t.boolean  "read",            default: false
    t.integer  "notifiable_id"
    t.string   "notifiable_type"
    t.integer  "notifier_id"
    t.string   "notifier_type"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_index "notifications", ["notifiable_id", "notifiable_type"], name: "index_notifications_on_notifiable_id_and_notifiable_type", using: :btree
  add_index "notifications", ["notifier_id", "notifier_type"], name: "index_notifications_on_notifier_id_and_notifier_type", using: :btree
  add_index "notifications", ["read"], name: "index_notifications_on_read", using: :btree
  add_index "notifications", ["user_id"], name: "index_notifications_on_user_id", using: :btree

  create_table "o_embed_caches", force: :cascade do |t|
    t.string   "url"
    t.text     "data"
    t.datetime "cache_age"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "o_embed_caches", ["cache_age"], name: "index_o_embed_caches_on_cache_age", using: :btree
  add_index "o_embed_caches", ["url"], name: "index_o_embed_caches_on_url", using: :btree

  create_table "photos", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "photoable_id"
    t.string   "photoable_type"
    t.text     "description"
    t.string   "image"
    t.integer  "width"
    t.integer  "height"
    t.string   "random_string"
    t.integer  "likes_count",    default: 0
    t.integer  "comments_count", default: 0
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "photos", ["comments_count"], name: "index_photos_on_comments_count", using: :btree
  add_index "photos", ["likes_count"], name: "index_photos_on_likes_count", using: :btree
  add_index "photos", ["photoable_id", "photoable_type"], name: "index_photos_on_photoable_id_and_photoable_type", using: :btree
  add_index "photos", ["user_id"], name: "index_photos_on_user_id", using: :btree

  create_table "places", force: :cascade do |t|
    t.string   "name"
    t.string   "formatted_address"
    t.string   "image"
    t.string   "formatted_phone_number"
    t.float    "rating"
    t.string   "website"
    t.string   "street_number"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.string   "postal_code"
    t.float    "longitude"
    t.float    "latitude"
    t.string   "google_place_id"
    t.string   "random_string"
    t.string   "slug"
    t.string   "route"
    t.string   "place_type"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "places", ["google_place_id"], name: "index_places_on_google_place_id", using: :btree
  add_index "places", ["slug"], name: "index_places_on_slug", unique: true, using: :btree

  create_table "pokemon_moves", force: :cascade do |t|
    t.integer  "pokemon_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "move_id"
  end

  add_index "pokemon_moves", ["move_id"], name: "index_pokemon_moves_on_move_id", using: :btree
  add_index "pokemon_moves", ["pokemon_id"], name: "index_pokemon_moves_on_pokemon_id", using: :btree

  create_table "pokemons", force: :cascade do |t|
    t.string   "name"
    t.string   "slug"
    t.string   "image"
    t.string   "random_string"
    t.decimal  "weight"
    t.decimal  "height"
    t.integer  "attack"
    t.integer  "defense"
    t.integer  "stamina"
    t.decimal  "capture_rate"
    t.decimal  "flee_chance"
    t.integer  "dodge_interval"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "type1"
    t.string   "type2"
    t.string   "pokedex"
  end

  add_index "pokemons", ["slug"], name: "index_pokemons_on_slug", unique: true, using: :btree

  create_table "pokestops", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "slug"
    t.integer  "user_id"
    t.datetime "hidden_at"
  end

  add_index "pokestops", ["slug"], name: "index_pokestops_on_slug", unique: true, using: :btree
  add_index "pokestops", ["user_id"], name: "index_pokestops_on_user_id", using: :btree

  create_table "posts", force: :cascade do |t|
    t.text     "body"
    t.string   "type"
    t.integer  "shares_count",     default: 0
    t.integer  "likes_count",      default: 0
    t.integer  "favorites_count",  default: 0
    t.integer  "comments_count",   default: 0
    t.integer  "user_id"
    t.integer  "shareable_id"
    t.string   "shareable_type"
    t.integer  "postable_id"
    t.string   "postable_type"
    t.integer  "o_embed_cache_id"
    t.integer  "sighting_id"
    t.integer  "photo_id"
    t.integer  "location_id"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "posts", ["comments_count"], name: "index_posts_on_comments_count", using: :btree
  add_index "posts", ["favorites_count"], name: "index_posts_on_favorites_count", using: :btree
  add_index "posts", ["likes_count"], name: "index_posts_on_likes_count", using: :btree
  add_index "posts", ["location_id"], name: "index_posts_on_location_id", using: :btree
  add_index "posts", ["o_embed_cache_id"], name: "index_posts_on_o_embed_cache_id", using: :btree
  add_index "posts", ["photo_id"], name: "index_posts_on_photo_id", using: :btree
  add_index "posts", ["postable_id", "postable_type"], name: "index_posts_on_postable_id_and_postable_type", using: :btree
  add_index "posts", ["shareable_id", "shareable_type", "user_id"], name: "index_shares_unique", unique: true, using: :btree
  add_index "posts", ["shareable_id"], name: "index_posts_on_root_id", using: :btree
  add_index "posts", ["shares_count"], name: "index_posts_on_shares_count", using: :btree
  add_index "posts", ["sighting_id"], name: "index_posts_on_sighting_id", using: :btree
  add_index "posts", ["type"], name: "index_posts_on_type", using: :btree
  add_index "posts", ["user_id"], name: "index_posts_on_user_id", using: :btree

  create_table "profiles", force: :cascade do |t|
    t.integer  "user_id"
    t.date     "birthday"
    t.string   "gender"
    t.text     "about"
    t.string   "first_name"
    t.string   "last_name"
    t.text     "snippet"
    t.string   "image"
    t.string   "random_string"
    t.integer  "location_id"
    t.integer  "team_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "profiles", ["location_id"], name: "index_profiles_on_location_id", using: :btree
  add_index "profiles", ["team_id"], name: "index_profiles_on_team_id", using: :btree
  add_index "profiles", ["user_id"], name: "index_profiles_on_user_id", using: :btree

  create_table "reports", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "reportable_type"
    t.integer  "reportable_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "reports", ["reportable_type", "reportable_id"], name: "index_reports_on_reportable_type_and_reportable_id", using: :btree
  add_index "reports", ["user_id"], name: "index_reports_on_user_id", using: :btree

  create_table "rsvps", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "rsvpable_id"
    t.string   "rsvpable_type"
    t.string   "status"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "rsvps", ["rsvpable_id", "rsvpable_type"], name: "index_rsvps_on_rsvpable_id_and_rsvpable_type", using: :btree
  add_index "rsvps", ["status"], name: "index_rsvps_on_status", using: :btree
  add_index "rsvps", ["user_id"], name: "index_rsvps_on_user_id", using: :btree

  create_table "sightings", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "pokemon_id"
    t.datetime "seen_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "expires_at"
    t.datetime "hidden_at"
  end

  add_index "sightings", ["pokemon_id"], name: "index_sightings_on_pokemon_id", using: :btree
  add_index "sightings", ["user_id"], name: "index_sightings_on_user_id", using: :btree

  create_table "teams", force: :cascade do |t|
    t.string   "name"
    t.string   "slug"
    t.string   "image"
    t.string   "random_string"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "teams", ["slug"], name: "index_teams_on_slug", unique: true, using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "username",               default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "slug"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree
  add_index "users", ["username"], name: "index_users_on_username", unique: true, using: :btree

  add_foreign_key "mailboxer_conversation_opt_outs", "mailboxer_conversations", column: "conversation_id", name: "mb_opt_outs_on_conversations_id"
  add_foreign_key "mailboxer_notifications", "mailboxer_conversations", column: "conversation_id", name: "notifications_on_conversation_id"
  add_foreign_key "mailboxer_receipts", "mailboxer_notifications", column: "notification_id", name: "receipts_on_notification_id"
  add_foreign_key "pokemon_moves", "pokemons"
end
