namespace :blog do 
  desc "Fetch Blog Posts"
  task fetch: :environment do
    url = "https://blog.catchemgo.com/latest"
    doc = Nokogiri::HTML(open(url))
    reversed_array = doc.css(".block").reverse
    reversed_array.each do |news|
      NewsFeed.where(
        title: news.at_css('h3').text,
        description: news.at_css('p').text,
        domain: URI.parse(url).host,
        full_url: news.at_css('article a')[:href]
      ).first_or_create(
        remote_image_url: news.at_css('.section-content img')[:src]
      )
    end
  end
end