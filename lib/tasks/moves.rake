namespace :moves do 
  desc "Fetch All Pokemon Moves"
  task fetch: :environment do
    master_json = "https://raw.githubusercontent.com/squarecat/pokemongo-data/master/assets/master_v0.31.0.json"
    data = JSON.load(open(master_json))
    moves = data['item_templates'].select {|k,v| k["move_settings"]}
    moves.each do |move_hash|
      m = move_hash["move_settings"]
      Move.find_or_create_by(
        name: m["vfx_name"].titleize.gsub(" Fast", ""),
        vfx_name: m["vfx_name"],
        power: m["power"].present? ? m["power"] : 0,
        energy: m["energy_delta"],
        dps: m["Power"].present? ? (1000.0/m["DurationMs"]) * m["Power"] : 0,
        accuracy: m["accuracy_chance"],
        critical_chance: m["critical_chance"].present? ? m["critical_chance"] : nil,
        duration: m["duration_ms"],
        move_type: m["pokemon_type"].split("TYPE_").last.capitalize,
        special: m["critical_chance"].present? ? true : false 
      )
    end
  end

  desc "Connect Moves with their Pokemon"
  task connect: :environment do
    master_json = "https://raw.githubusercontent.com/squarecat/pokemongo-data/master/assets/master_v0.31.0.json"
    data = JSON.load(open(master_json))
    pokemon = data["item_templates"].select{|k,v| k["pokemon_settings"]}
    pokemon.each do |pokemon_hash|
      hash = pokemon_hash["pokemon_settings"]
      p = Pokemon.find_by(name: hash['pokemon_id'].capitalize)
      hash['quick_moves'].each do |q|
        move = Move.find_by(vfx_name: q.downcase)
        p.moves << move
      end

      hash['cinematic_moves'].each do |s|
        move = Move.find_by(vfx_name: s.downcase)
        p.moves << move
      end
    end
  end
end