namespace :pokemon do 
  desc "Fetch All Pokemon"
  task fetch: :environment do
    master_json = "https://raw.githubusercontent.com/squarecat/pokemongo-data/master/assets/master_v0.31.0.json"
    image_url = "http://serebii.net/pokemongo/pokemon/"
    data = JSON.load(open(master_json))
    pokemon = data['item_templates'].select {|k,v| k["pokemon_settings"]}
    pokemon.each do |pokemon_hash|
      t = pokemon_hash["template_id"]
      p = pokemon_hash["pokemon_settings"]
      Pokemon.where(
        name: p['pokemon_id'].capitalize,
        pokedex: t.split("_POKEMON_")[0].split("V0").last,
        weight: p["pokedex_weight_kg"],
        height: p["pokedex_height_m"],
        attack: p["stats"]["base_attack"],
        defense: p["stats"]["base_defense"],
        stamina: p["stats"]["base_stamina"],
        capture_rate: p["encounter"]["base_capture_rate"],
        flee_chance: p["encounter"]["base_flee_rate"],
        dodge_interval: p["encounter"]["movement_timer_s"],
        type1: p["type"].split("TYPE_").last.titleize,
        type2: p["type_2"].present? ? p["type_2"].split("TYPE_").last.titleize : nil
      ).first_or_create(
        remote_image_url: image_url + t.split("_POKEMON_")[0].split("V0")[1] + ".png"
      )
    end
  end
end