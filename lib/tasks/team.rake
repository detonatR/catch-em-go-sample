namespace :teams do 
  desc "Fetch All Pokemon"
  task create: :environment do
    Team.create(
      name: "Valor", 
      remote_image_url: "https://raw.githubusercontent.com/Superviral/Pokemon-GO-App-Assets-and-Images/master/App%20Converted%20Images/team_red.png"
    )

    Team.create(
      name: "Mystic", 
      remote_image_url: "https://raw.githubusercontent.com/Superviral/Pokemon-GO-App-Assets-and-Images/master/App%20Converted%20Images/team_blue.png"
    )

    Team.create(
      name: "Instinct", 
      remote_image_url: "https://raw.githubusercontent.com/Superviral/Pokemon-GO-App-Assets-and-Images/master/App%20Converted%20Images/team_yellow.png"
    )
  end
end